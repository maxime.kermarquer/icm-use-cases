#!/bin/bash
#SBATCH --job-name=ketabi_v0044
#SBATCH --time=2-00:00:00
#SBATCH --partition=normal
#SBATCH --mem=32G
#SBATCH --cpus-per-task=28
#SBATCH --chdir=.
#SBATCH --output=outputs/ketabi_v0044_matlab_%j.txt
#SBATCH --error=outputs/ketabi_v0044_matlab_%j.txt


echo -e "--- \e[1;34mSetting up environment\e[0m ---"
module load MATLAB

echo -e "--- \e[1;34mStart\e[0m ---"

echo -e "\e[33mmatlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/ketabiFaceAdvice_v0044.m\e[0m"
matlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/ketabiFaceAdvice_v0044.m

echo -e "--- \e[1;34mEnd\e[0m ---"

exit 0
