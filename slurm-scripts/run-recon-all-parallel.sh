#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=28
#SBATCH --mem=8G
#SBATCH --time=1-00:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/use-case-freesurfer-parallel-%j.txt
#SBATCH --error=outputs/use-case-freesurfer-parallel-%j.txt
#SBATCH --job-name=use-case-freesurfer-parallel

echo -e "--- \e[1;34mSetting up environment\e[0m ---"

module load FreeSurfer/6.0.0
# FreeSurfer installation :
# https://surfer.nmr.mgh.harvard.edu/fswiki/rel6downloads


# Documentation recon-all :
# https://surfer.nmr.mgh.harvard.edu/fswiki/recon-all
INPUT_DATA=data/mri/patient_1_t1_mp2rage_sag_p3_1mm_iso.nii
RESULTS_DIRECTORY=results
SUBJECT=PATIENT_1
RECON_ALL_OPTIONS="-all -brainstem-structures -norandomness -parallel -openmp ${SLURM_CPUS_PER_TASK}"



echo -e "--- \e[1;34mStart\e[0m ---"


echo -e "\e[33mrecon-all  -i ${INPUT_DATA} -s ${SUBJECT} -sd ${RESULTS_DIRECTORY} ${RECON_ALL_OPTIONS}\e[0m"
recon-all  -i ${INPUT_DATA} -s ${SUBJECT} -sd ${RESULTS_DIRECTORY} ${RECON_ALL_OPTIONS}

echo -e "--- \e[1;34mEnd\e[0m ---"

exit 0
