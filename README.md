
# ICM use cases

The scripts to launch the use cases are in `slurm-scripts` directory.

```bash
sbatch slurm-scripts/run-ketabi_v0044.sh
sbatch slurm-scripts/run-recon-all-parallel.sh
```

---


## MATLAB job for clinical data

The MATLAB script `matlab-scripts/ketabiFaceAdvice_v0044.m` fits behavioral data to determine, for each individual, the parameters that most likely to produce that behavior.

It will get the inputs and launching (by parallelizing the subjects, with a `parfor`) the functions that will determine the probability distribution on each parameter and converge to the most probable set of parameters given the product behavior and the priors we had on the parameters.

### Sotfware prerequisites

* MATLAB version >= R2017b
* https://fr.mathworks.com/products/matlab.html

### Input data

In the directory `data/clinic`, each MATLAB data file is the results of an individual on a behavioral test.

### Output data

In the directory `results/ketabiFaceAdvice_v0044`, each MATLAB data file contains the parameters obtained for each individual.

You can find reference results in [here](https://owncloud.icm-institute.org/index.php/s/hpukA7qbESVsY6p).

### Computing time and resources

* Executed on **28 cores** (on a node with 2 Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz) 
* **~22G** of memory used
* **~1-05:30:00** of computing time *(days-hours:minutes:seconds)*

---


## MRI processing

> FreeSurfer is a software package that enables you to analyze structural MRI images - in other words, you can use FreeSurfer to quantify the amount of grey matter and white matter in specific regions of the brain.

> Recon-all stands for reconstruction, as in reconstructing a two-dimensional cortical surface from a three-dimensional volume. The images we collect from an MRI scanner are three-dimensional blocks, and these images are transformed by recon-all into a smooth, continuous, two-dimensional surface - similar to taking a paper lunch bag crumpled into the size of a pellet, and then blowing into it to expand it like a balloon.

![](docs/reconstruction.png)

> The command recon-all converts a three-dimensional anatomical volume (shown on the left, represented by a typical sagittal slice taken through a volume) into a two-dimensional surface (right).

More information : 

* https://surfer.nmr.mgh.harvard.edu/fswiki/recon-all
* https://andysbrainbook.readthedocs.io/en/latest/FreeSurfer/FS_ShortCourse/FS_03_ReconAll.html

### Sotfware prerequisites

* FreeSurfer 6 Release 
* https://surfer.nmr.mgh.harvard.edu/fswiki/rel6downloads

### Input data

A MRI as NIFTI file or DICOM file.

### Output data

In the directory `results/PATIENT_1`

* https://surfer.nmr.mgh.harvard.edu/fswiki/ReconAllOutputFiles
* https://andysbrainbook.readthedocs.io/en/latest/FreeSurfer/FS_ShortCourse/FS_03_ReconAll.html#the-output-of-recon-all

You can find reference results in directory `reference-results/PATIENT_1`.

To visualize results :

```bash
freeview -v  mri/T1.mgz mri/wm.mgz mri/brainmask.mgz mri/aseg.mgz:colormap=lut:opacity=0.2 -f surf/lh.white:edgecolor=blue surf/lh.pial:edgecolor=red surf/rh.white:edgecolor=blue surf/rh.pial:edgecolor=red
```

### Computing time and resources

* Executed on **28 cores** (on a node with 2 Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz) 
* **~7G** of memory used
* **~12:00:00**  of computing time 


