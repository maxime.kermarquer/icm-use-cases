function [dim, options] = setListPriors(options, dim, priorType, listName, mu, sigma)


nP = length(listName);
if length(mu) == 1
    mu = repmat(mu, nP, 1);
end
   
if length(sigma) == 1
    sigma = repmat(sigma, nP, 1);
end
sigma = diag(sigma);
for i=1:nP
    labeler.(listName{i}) = i;
end


    switch priorType
        case 'theta'
            [options.priors.muTheta,options.priors.SigmaTheta, options.inF.thetaLabel] = deal(mu, sigma, labeler);  
            nTheta=length(options.priors.muTheta);
            dim.n_theta = nTheta;
        case 'phi'
            [options.priors.muPhi,options.priors.SigmaPhi, options.inG.phiLabel] =  deal(mu, sigma, labeler);  
            nPhi = length(options.priors.muPhi);
            dim.n_phi = nPhi;            
        case 'X0'
            [options.priors.muX0,options.priors.SigmaX0, stateLabel] = deal(mu, sigma, labeler);  
            options.inF.stateLabel =  stateLabel;
            options.inG.stateLabel =  stateLabel;
            nX0 = length(options.priors.muX0);
            dim.n = nX0;
    end