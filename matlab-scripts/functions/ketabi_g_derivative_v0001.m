function [dgdx,dgdp] = ketabi_g_derivative_v0001(gx,inG,matX)

isSoftmaxPx = 0;

%% dgdx
sizeDgdx = [length(matX) size(gx,1)];
dgdx = zeros(sizeDgdx);

% confidence
dgdx(11,1) = 3;

%% dgdp
sizeDgdp = [1 length(gx)];
dgdp = zeros(sizeDgdp);

end

