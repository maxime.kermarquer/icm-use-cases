function fx = f_ketabi(x, data)

% x est  priorTarget
% u est advicePosition
fx = x;
fx(prevActionIdx) = (x(prevActionIdx) .* u(prevActionIdx)) ./ sum(x(prevActionIdx), u(prevActionIdx));

% x is position of the advice, existing on [1:51]
xScreen = -7:59;
sigmaTarget = 12;    % uncertainty of target position prior to advice
sigmaAdvice = 4.8;    % uncertainty of advice (is it here or there?)

% Distributions initiales
priorTarget = normpdf(xScreen, (abs(max(xScreen)) - abs(min(xScreen))) / 2, sigmaTarget);
rangeAdvice = sum(normpdf(1:1000, 0, sigmaAdvice) > .02);

% Mise � jour du prior 
priorTarget = (priorTarget .* data.tableData.finalPosition) ./ sum(priorTarget, data.tableData.finalPosition);

% Evidence perceptive de l'indice
evidenceAdvice_positive = zeros(1, length(xScreen));
evidenceAdvice_positive(x : x + 2 * rangeAdvice) = normpdf((-rangeAdvice) : (rangeAdvice), 0, sigmaAdvice);
evidenceAdvice_positive = evidenceAdvice_positive / sum(evidenceAdvice_positive);
evidenceAdvice_negative(evidenceAdvice_positive == 0) = 1;
evidenceAdvice_negative(evidenceAdvice_positive ~= 0) = 0;
evidenceAdvice_negative = evidenceAdvice_negative / sum(evidenceAdvice_negative);

% Localisation post�rieure de la cible
postTarget = .5 * evidenceAdvice_positive .* priorTarget + .5 * evidenceAdvice_negative .* priorTarget;
fx = postTarget / sum(postTarget); 


end
