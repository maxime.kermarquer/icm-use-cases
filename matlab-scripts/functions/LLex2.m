function [LL,dLLdSigma,dLLdTemperatureAdvice] = LLex2(e,t,y,grid)
% log-likelihood of e and x: log p(y|x,e)
% function [LL] = LLex(e,x,y,P)
% IN:
%   - e: the values of e at which log p(y|x,y) is evaluated
%   - t: the values of t at which log p(y|x,y) is evaluated
%   - y: the values of y at which log p(y|x,y) is evaluated
%   - P: parameter structure
% OUT:
%   - LL: log-likelihood: log p(y|x,e)

% benevolent likelihood function: log p(y|x,Inf)
lp1 = logGauss(y,t,grid.sigma,grid.d.y);
p1 = exp(lp1-max(lp1(:)))*exp(max(lp1(:))); % for numerical stability
dP1dSigma = ((t-y-sqrt(grid.sigma)).*(t-y+sqrt(grid.sigma))/grid.sigma).*p1;

alpha = grid.alpha;
beta = grid.beta;
dAlphadSigma = grid.dAlphadSigma;
dBetadSigma = grid.dBetadSigma;

% p(y|x,e) and log(p(y|x,e))
sigE =  1 ./ (1 + exp(-e*grid.slope));
py = sigE*p1 + (1-sigE)*(alpha-beta*p1);
LL = log(py);

% dp(y|x,e)/dSigmaAdvice and then d(log(py))dSigma
dPydSigma = sigE.*dP1dSigma + (1 - sigE).*(dAlphadSigma - dBetadSigma*p1 - dP1dSigma*beta);
dLLdSigma = dPydSigma./py;

% dp(y|x,e)/dSlopeAdvice and then d(log(py))dSlope
dSigEdTemperatureAdvice = (sigE.*(1-sigE)).*e*grid.slope;
dPydTemperatureAdvice = dSigEdTemperatureAdvice .* ((beta+1)*p1-alpha);
dLLdTemperatureAdvice = dPydTemperatureAdvice./py;

end


