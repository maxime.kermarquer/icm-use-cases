function [ orderedVector, sortIndex, rank ] = sort2( varargin )
%% Use sort function to return rank of elements on top of usual outputs of sort

[orderedVector, sortIndex]= sort(varargin{:});
rank = 1:length(sortIndex);
rank(sortIndex) = rank;

end

