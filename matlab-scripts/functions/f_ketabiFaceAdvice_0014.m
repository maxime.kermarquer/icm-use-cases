function [updatedX,dfdx,dfdp] = f_ketabiFaceAdvice_0014(x_t,P,u_t,inF)
%% function [updatedX] = f_ketabiFaceAdvice_0014(x_t,P,u_t,inF)
%
% INPUT
% - x_t : 
%           muFace_(faceIdentity)     = mean of the prior distribution for faceIdentity
%           sigmaFace_(faceIdentity)  = variance of the prior distribution for faceIdentity
%           muTarget                  = mean of the prior distribution for target position
%           sigmaTarget               = variance of the prior distribution for target position
%           confidence                = confidence derived from the shooting position, to be carried out to g_
%           bin(i)                    = posterior distribution of target position divided in nBin bins
% - P : 
%             muFace_0              = mean of prior on the intention of people in general
%             sigmaFace_0           = std of prior on the intention of people in general
%             sigmaAdvice           = precision of the advice(range where the advice gives information)
%             sigmaFace_percept     = precision of the emotion depending of the facial expression
%             temperatureAdvice     = temperature of the sigmoid used to transform e into b
%             gamma                 = precision of the prior of the emotion displayed by an individual
%             temperatureConfidence = temperature of the dependance between confidence and probability of success
%             shiftConfidence       = shift of the dependance between confidence and probability of success
% - u_t :
%           advicePosition       
%           identityFace     
%           degreeExpression       
%           iTrial                      
% - inF : 
%           listFaceIdentity : a cell with all face identitity in the correct order (faceNumber)
%           nBin : number of bins to divide the posterior distribution of the target position
%           isFinalPosition = use real shooting position to derive confidence if (1), use best simulated position if (0)
% OUTPUT
% - fx :   Updated X



%% Get parameters and u (experimental conditions)
%--------------------------------------------------------------------------
[X,theta,u] = getStateParamInput(x_t,P, u_t,inF);
currentFace = inF.listFaceIdentity{u.identityFace};

%% Tansform free parameters
%--------------------------------------------------------------------------
muFace_0 = theta.muFace_0;
sigmaFace_0 = exp(theta.sigmaFace_0);
sigmaAdvice = exp(theta.sigmaAdvice);
sigmaFace_percept = exp(theta.sigmaFace_percept);
temperatureAdvice = exp(theta.temperatureAdvice);
gamma = exp(theta.gamma);
temperatureConfidence = exp(theta.temperatureConfidence);
shiftConfidence = theta.shiftConfidence;

%% set model parameters
% initialize grids 
grid.d.x    = 1; % x: grid step size
grid.d.e    = 1e-1; % e: grid step size
grid.d.mu   = 1e-1; % mu: grid step size
grid.d.y    = 1e0; % y: grid step size
grid.d.f    = 1e-1; % f: grid step size
grid.gx     = -11:grid.d.x:63; % x: grid
grid.ge     = -10:grid.d.e:10; % e: grid
grid.gmu    = -5:grid.d.mu:5; % mu: grid
grid.gy     = -11:grid.d.y:63;  % used to determine alpha and beta

% model parameters
grid.muTarget       = X.muTarget;
grid.sigmaTarget    = (exp(X.sigmaTarget))^2;
grid.gamma          = gamma^2; 
grid.ohmega         = sigmaFace_percept^2; 
grid.sigma          = sigmaAdvice^2;
grid.slope          = temperatureAdvice;      

% parameters alpha and beta are determined to build the right inverse gaussian
py0 = exp(logGauss(grid.gy,27,grid.sigma,grid.d.y));
coeffSoftmax = 1e3;
softmaxPy0 = log(sum(exp(coeffSoftmax*py0)))/coeffSoftmax;
grid.alpha = 1/sum(1-py0/softmaxPy0);
grid.beta = 1/(sum(1-py0/softmaxPy0)*softmaxPy0);

dPy0dSigma = ((grid.gy-27-sqrt(grid.sigma)).*(grid.gy-27+sqrt(grid.sigma))/grid.sigma).*py0;
dSoftmaxdSigma = 1/coeffSoftmax * 1/sum(exp(coeffSoftmax*py0)) * (sum(coeffSoftmax*dPy0dSigma.*exp(coeffSoftmax*py0)));
grid.dAlphadSigma = sum((dPy0dSigma.*softmaxPy0 - py0.*dSoftmaxdSigma)/softmaxPy0^2) * 1 / (sum(1-py0/softmaxPy0)^2);

w = sum(1-py0/softmaxPy0);
v = softmaxPy0;
dwdSigma = -sum((dPy0dSigma.*softmaxPy0 - dSoftmaxdSigma.*py0)/dSoftmaxdSigma^2);
dvdSigma = dSoftmaxdSigma;
grid.dBetadSigma = -(dwdSigma.*v + dvdSigma.*w)/(w.*v)^2;
% grid.dBetadSigma = (grid.dAlphadSigma*softmaxPy0 - grid.alpha*dSoftmaxdSigma) / softmaxPy0^2;

% set prior density on mu
if u.iTrial == 1
    for iFace = 1:length(inF.listFaceIdentity)
        X.(['muFace_' inF.listFaceIdentity{iFace}]) = muFace_0;
        X.(['sigmaFace_' inF.listFaceIdentity{iFace}]) = sigmaFace_0;
    end
end
muFace = X.(['muFace_' currentFace]);
sigmaFace = X.(['sigmaFace_' currentFace]);
P0mu = normpdf(grid.gmu, X.(['muFace_' currentFace]), X.(['sigmaFace_' currentFace]));
P0mu = P0mu./sum(P0mu);
P0mu(P0mu==0)=eps;

%% compute pmu and px
% form 3D-grids...
GX = repmat(grid.gx(:),[1,length(grid.ge),length(grid.gmu)]);
GE = repmat(grid.ge(:)',[length(grid.gx),1,length(grid.gmu)]);
GMU = repmat(permute(grid.gmu(:),[2,3,1]),[length(grid.gx),length(grid.ge),1]);
% ... and 3D-prior
P0MU = repmat(permute(P0mu(:),[2,3,1]),[length(grid.gx),length(grid.ge),1]);
% evaluate p(x,e,mu,y,f)
[LJ,dLLexydSigma,dLLdTemperatureAdvice] = logJoint(GX,GE,GMU,log(P0MU),u.advicePosition,u.degreeExpression,grid);
JP = exp(LJ-max(LJ(:))).*exp(max(LJ(:)));
dJPdSigma = JP.*dLLexydSigma;
dJPdTemperatureAdvice = JP.*dLLdTemperatureAdvice;

% marginalize p(x,e,mu|y,f) to get posterior beliefs on x and mu
px = sum(sum(JP,3),2);
px = px./sum(px);

pmu = VBA_vec(sum(sum(JP,2),1));
pmu = pmu./sum(pmu);

X.(['muFace_' currentFace]) = grid.gmu * pmu;
X.(['sigmaFace_' currentFace]) = sqrt((grid.gmu).^2 * pmu - (grid.gmu * pmu) ^2);

%% bin it all
% px
nBin = inF.nBin; % number of bins
minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
rangePosition = -11:63; % possible positions on screen during experiment
binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential

for iBin = 1:nBin
    X.(['bin',num2str(iBin)]) = sum(px(binPosition(iBin) : binPosition(iBin + 1)));
end

%% derive confidence
rangePosition = -11:63; % possible positions on screen during experiment
shootPosition = u.finalPosition + abs(rangePosition(1)) + 1;    % from real shooting position
shootConfidence = px(round(shootPosition));
X.confidence = 1 / (1 + exp((-shootConfidence + shiftConfidence) / temperatureConfidence));

[dfdx,dfdp] = ketabi_f_derivative_v0014(muFace,sigmaFace,X,theta,u,grid,inF,GX,GE,GMU,JP,dJPdSigma,dJPdTemperatureAdvice,shootPosition,shootConfidence);

%% Update
updatedX = table2array(struct2table(X))';

