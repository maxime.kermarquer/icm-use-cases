function [gx,dgdx,dgdp] = g_ketabiFaceAdvice_0003(x_t,P,u_t,inG)
%%function [gx] = g_ketabiFaceAdvice_0003(x_t,P,u_t,inG)
%
% INPUT
% - x_t : 
%           muFace_(faceIdentity)     = mean of the prior distribution for faceIdentity
%           sigmaFace_(faceIdentity)  = variance of the prior distribution for faceIdentity
%           confidence                = confidence derived from the shooting position, to be carried out to g_
%           bin(i)                    = posterior distribution of target position divided in nBin bins
% - P : 
%           temperatureShoot          = temperature of the dependance between choice of shoot position and probability of success
% - u_t :
%           advicePosition       
%           identityFace     
%           degreeExpression       
%           iTrial                      
% - inG :
%           listFaceIdentity : a cell with all face identitity in the correct order (faceNumber)
%           nBin : number of bins to divide the posterior distribution of the target position
%           isConfidence : is confidence fit (1) or not(0)
%           isSoftmaxPx : use softmax to choose the best shooting position according to the probability of success if (1), use raw probability if (0)         
% OUTPUT
% - gx :   both moments of the posterior

%% Get parameters and u (experimental conditions)
%--------------------------------------------------------------------------
[X, phi,u] = getStateParamInput(x_t,P, u_t,inG);
% temperatureShoot = exp(phi.temperatureShoot);
temperatureShoot = 1;

% shooting position information
matX = table2array(struct2table(X));
isSoftmaxPx = 0;
if  isSoftmaxPx == 0
    gx = matX;
elseif isSoftmaxPx == 1
    gx = exp(matX' / temperatureShoot) ./ sum(exp(matX' / temperatureShoot));
end

[dgdx,dgdp] = ketabi_g_derivative_v0003(gx,inG,matX,temperatureShoot,isSoftmaxPx);

end

