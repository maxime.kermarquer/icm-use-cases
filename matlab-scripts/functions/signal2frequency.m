function [ f, P1, t ] = signal2frequency( X, T )
%% This is a wrapper to extract the frequency spectrum of a signal
%  X  is the signal
%  Fs is the sampling period (default = 1)
%  f are the frequencies
%  P1 is the power associated to each frequency
%  t is the periods (=1/f)

if nargin == 1
    T=1;
end

Fs = 1/T;             % Sampling frequency     
L = length(X);        % Length of signal
t = (0:L-1)*T;        % Time vector

Y = fft(X);
P2 = abs(Y/L);
P1 = P2(1:ceil(L/2)+1);
P1(2:end-1) = 2*P1(2:end-1);

f = Fs*(0:(L/2))/L;
t = 1./f;

end

