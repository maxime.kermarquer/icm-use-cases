function [dgdx,dgdp] = ketabi_g_derivative_v0004(gx,inG,u,x_t)

%% dgdx
sizeDgdx = [length(x_t) length(gx)];
dgdx = zeros(sizeDgdx);

% confidence
dgdx(13,1) = 5;

%% dgdp
sizeDgdp = [1 length(gx)];
dgdp = zeros(sizeDgdp);

end

