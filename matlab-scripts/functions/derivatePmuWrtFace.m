function [dMuFacedFace,dSigmaFacedFace] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,isSigma)
    
    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu

    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace)./sigmaFace^2),1),2));
    elseif isSigma==1
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace))/sigmaFace^3,1),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPmu = (dDoubleSumJP*tripleSumJP - dTripleSumJP*doubleSumJP) / tripleSumJP^2;

    dMuFacedFace = grid.gmu * dNormalizedPmu;  
    dSigmaFacedFace = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
            / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));
        
end

