function s=sortByLabel(labeler,muPriors, s)

if nargin < 2
    s = struct ;
end

names = fieldnames(labeler);

% s=[];
for i=1:numel(names)
    try
        s.(names{i})(end+1) = muPriors(labeler.(names{i}),:)';
    catch
        s.(names{i}) = muPriors(labeler.(names{i}),:)';
    end
    
end