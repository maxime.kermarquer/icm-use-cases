function [ newV ] = changeLabel( V, newLabel, oldLabel )
%% This function is a home-made version of renamecats that changes the values of a vector / factor
% function [ newV ] = changeLabel( V, newLabel, oldLabel )

if islogical(V)
    V = +V;
end
V = nominal(V);
label = unique(V, 'stable');
if nargin == 3
    oldLabel = nominal(oldLabel);
else
    oldLabel = label;
end

nOld = length(oldLabel);
if nargin == 1
    newLabel = 1:nOld;
end

newLabel=nominal(newLabel);

for iOld = 1:nOld
    newV(V==oldLabel(iOld)) = newLabel(iOld);
end

newV = reshape(newV, size(V));

end

