function [dfdx,dfdp] = ketabi_f_derivative_v0024(muFace,sigmaFace,X,theta,u,grid,inF,GX,GE,GMU,JP,dJPdSigma,dJPdTemperatureAdvice,shootPosition,shootConfidence)


currentFace = inF.listFaceIdentity{u.identityFace};
muFace_0 = theta.muFace_0;
sigmaFace_0 = exp(theta.sigmaFace_0);
sigmaAdvice = exp(theta.sigmaAdvice);
sigmaFace_percept = exp(theta.sigmaFace_percept);
temperatureAdvice = exp(theta.temperatureAdvice);
gamma = exp(theta.gamma);
interTrialNoise = exp(theta.interTrialNoise);
temperatureConfidence = exp(theta.temperatureConfidence);
shiftConfidence = theta.shiftConfidence;
nBin = inF.nBin;

%% dfdx
sizeDfdx = length(fieldnames(X));
dfdx = zeros(sizeDfdx);

% derivative w.r.t. muFace
if u.iTrial~=1
    for iFace=1:10
        dfdx(iFace,iFace)=1;
    end
    [dfdx(u.identityFace,u.identityFace),dfdx(u.identityFace,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    dfdx(u.identityFace,end-nBin:end) = dShootPxWrtFace(muFace,sigmaFace,GMU,JP,inF,0,shootPosition,X,temperatureConfidence);
end

% w.r.t sigmaFace
if u.iTrial~=1
    [dfdx(u.identityFace+5,u.identityFace),dfdx(u.identityFace+5,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    dfdx(u.identityFace+5,end-nBin:end) = dShootPxWrtFace(muFace,sigmaFace,GMU,JP,inF,1,shootPosition,X,temperatureConfidence);
end

% w.r.t. muTarget
dfdx(11,11)=1;
[dfdx(11,u.identityFace), dfdx(11,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,0);
dfdx(11,end-nBin:end) = dShootPxWrtTarget(X,GX,JP,inF,0,shootPosition,temperatureConfidence);

% w.r.t. sigmaTarget
dfdx(12,12)=1;
[dfdx(12,u.identityFace), dfdx(12,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,1);
dfdx(12,end-nBin:end) = dShootPxWrtTarget(X,GX,JP,inF,1,shootPosition,temperatureConfidence);

%% dfdp
sizeDfdp = length(fieldnames(theta));
dfdp = zeros(sizeDfdp,sizeDfdx);

% w.r.t. muFace_0
if u.iTrial == 1
    dfdp(1,1:5) = 1;
    [dfdp(1,u.identityFace), dfdp(1,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    dfdp(1,end-nBin:end) = dShootPxWrtFace(muFace,sigmaFace,GMU,JP,inF,0,shootPosition,X,temperatureConfidence);
end
    
% w.r.t. sigmaFace_0
if u.iTrial == 1
    dfdp(2,6:10) = 1;
    [dfdp(2,u.identityFace), dfdp(2,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    dfdp(2,end-nBin:end) = dShootPxWrtFace(muFace,sigmaFace,GMU,JP,inF,1,shootPosition,X,temperatureConfidence);
end

% w.r.t. sigmaAdvice 
[dfdp(3,u.identityFace), dfdp(3,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdSigma,grid);
dfdp(3,end-nBin:end) = dShootPxWrtAdvice(JP,dJPdSigma,inF,shootPosition,X,temperatureConfidence);

% w.r.t. sigmaFacePercept
[dfdp(4,u.identityFace), dfdp(4,u.identityFace+5)] = derivatePmuWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,grid,1);
dfdp(4,end-nBin:end) = dShootPxWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,inF,1,shootPosition,X,temperatureConfidence);

% % w.r.t. slope (inverse temperature) of advice
[dfdp(5,u.identityFace), dfdp(5,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdTemperatureAdvice,grid);
dfdp(5,end-nBin:end) =  dShootPxWrtAdvice(JP,dJPdTemperatureAdvice,inF,shootPosition,X,temperatureConfidence);

% w.r.t. gamma (intentional variability)
[dfdp(6,u.identityFace), dfdp(6,u.identityFace+5)] = derivatePmuWrtFace(GMU,gamma,GE,JP,grid,1);
dfdp(6,end-nBin:end) = dShootPxWrtFace(GMU,gamma,GE,JP,inF,1,shootPosition,X,temperatureConfidence);

% w.r.t. intertrial noise
dfdp(7,u.identityFace+5) = interTrialNoise;

% wrt confidence temperature
dfdp(8,end-nBin) = -X.confidence*(1-X.confidence)*(shootConfidence-shiftConfidence)/temperatureConfidence;
dfdp(9,end-nBin) = -X.confidence*(1-X.confidence)/temperatureConfidence;

end

function [derivatives] = dShootPxWrtFace(muFace,sigmaFace,GMU,JP,inF,isSigma,shootPosition,X,temperatureConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace)./sigmaFace^2),3),2));
    elseif isSigma==1
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace))/sigmaFace^3,3),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end

function [derivatives] = dShootPxWrtTarget(X,GX,JP,inF,isSigma,shootPosition,temperatureConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    if isSigma==0 % then we derivate wrt mean
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GX-X.muTarget)./(exp(X.sigmaTarget))^2),3),2));
    elseif isSigma==1 % then we derivate wrt sigma
        dDoubleSumJP=VBA_vec(sum(sum(JP*exp(X.sigmaTarget).*(GX-X.muTarget-exp(X.sigmaTarget)).*(GX-X.muTarget+exp(X.sigmaTarget))./exp(3*X.sigmaTarget),3),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
    
    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end

function [derivatives] = dShootPxWrtAdvice(JP,dJPdTheta,inF,shootPosition,X,temperatureConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP, 3),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(dJPdTheta, 3),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
    
    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end


