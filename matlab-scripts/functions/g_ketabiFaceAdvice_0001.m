function [gx,dgdx,dgdp] = g_ketabiFaceAdvice_0001(x_t,P,u_t,inG)
%%function [gx] = g_ketabiFaceAdvice_0028(x_t,P,u_t,inG)
%
% INPUT
% - x_t : 
%           muFace_(faceIdentity)     = mean of the prior distribution for faceIdentity
%           sigmaFace_(faceIdentity)  = variance of the prior distribution for faceIdentity
%           confidence                = confidence derived from the shooting position, to be carried out to g_
%           bin(i)                    = posterior distribution of target position divided in nBin bins
% - u_t :
%           advicePosition       
%           identityFace     
%           degreeExpression       
%           iTrial                      
% - inG :
%           listFaceIdentity : a cell with all face identitity in the correct order (faceNumber)
%           nBin : number of bins to divide the posterior distribution of the target position
%           isConfidence : is confidence fit (1) or not(0)
% OUTPUT
% - gx :   both moments of the posterior

%% Get parameters and u (experimental conditions)
%--------------------------------------------------------------------------
[X, phi,u] = getStateParamInput(x_t,P, u_t,inG);

% confidence information
gx = 1 + X.confidence * 3;
matX = table2array(struct2table(X));

[dgdx,dgdp] = ketabi_g_derivative_v0001(gx,inG,matX);

end

