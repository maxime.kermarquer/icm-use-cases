function [gx,dgdx,dgdp] = g_ketabiFaceAdvice_0004(x_t,P,u_t,inG)
%
% INPUT
% - x_t : 
%           muFace_(faceIdentity)     = mean of the prior distribution for faceIdentity
%           sigmaFace_(faceIdentity)  = variance of the prior distribution for faceIdentity
%           muTarget                  = mean of the prior distribution for target position
%           sigmaTarget               = variance of the prior distribution for target position
%           confidence                = confidence derived from the shooting position, to be carried out to g_
%           bin(i)                    = posterior distribution of target position divided in nBin bins
% - P : 
%           temperatureShoot          = temperature of the dependance between choice of shoot position and probability of success
% - u_t :
%           advicePosition       
%           identityFace     
%           degreeExpression       
%           iTrial                      
% - inG :
%           listFaceIdentity : a cell with all face identitity in the correct order (faceNumber)
% OUTPUT
% - gx :    confidence rating

%% Get parameters and u (experimental conditions)
%--------------------------------------------------------------------------
[X, phi,u] = getStateParamInput(x_t,P, u_t,inG);

% confidence information
gx = 5 * X.confidence;

[dgdx,dgdp] = ketabi_g_derivative_v0004(gx,inG,u,x_t);

end

