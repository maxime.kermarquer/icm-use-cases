function [ wPtr, color, windowRect, xCenter, yCenter, screenXpixel, screenYpixel ] = psychtoolbox_screen( isFullScreen, isSkip )
%% [ wPtr, color, windowRect, xCenter, yCenter, screenXpixel, screenYpixel ] = psychtoolbox_screen( isFullScreen, isSkip )
%  This is a wrapper to launch a psychotoolbox window, define colors and store screen dimensions

if ~exist('isSkip')
    isSkip = 1;
end

if ~exist('isFullScreen')
    isFullScreen = 0;
end

PsychDefaultSetup(2); % Here we call some default settings for setting up Psychtoolbox
if isSkip == 1
    Screen('Preference', 'SkipSyncTests', 2); % Skip sync tests
end

screens = Screen('Screens'); % Get the screen numbers
screenNumber = max(screens); % Draw to the external screen if avaliable

color.white = [255 255 255];   % Define black and white
color.black = [0 0 0];
color.gray= [200 200 200];
color.back=color.black;

if isFullScreen == 1
    rect = [];
else
    rect = [100 50 1124 818];
end

%% Configure screen
[wPtr, windowRect] = Screen('OpenWindow', screenNumber, color.back, rect); % Open an on screen window
Screen('BlendFunction',wPtr, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'); % Set up alpha-blending for smooth (anti-aliased) lines
[xCenter, yCenter] = RectCenter(windowRect);                    % Get the centre coordinate of the window
[screenXpixel, screenYpixel] = Screen('WindowSize', wPtr);   % Get the size of the on screen window



%% Configure text display
Screen('TextFont', wPtr, 'Arial');                  % Font and text size settings 
Screen('TextSize', wPtr, 36);

end

