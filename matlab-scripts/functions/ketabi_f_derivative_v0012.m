function [dfdx,dfdp] = ketabi_f_derivative_v0012(muFace,sigmaFace,X,theta,u,grid,inF,GX,GE,GMU,JP,dJPdSigma,dJPdTemperatureAdvice,varConfidence)


currentFace = inF.listFaceIdentity{u.identityFace};
muFace_0 = theta.muFace_0;
sigmaFace_0 = exp(theta.sigmaFace_0);
sigmaAdvice = exp(theta.sigmaAdvice);
sigmaFace_percept = exp(theta.sigmaFace_percept);
temperatureAdvice = exp(theta.temperatureAdvice);
gamma = exp(theta.gamma);
temperatureConfidence = exp(theta.temperatureConfidence);
shiftConfidence = theta.shiftConfidence;

%% dfdx
sizeDfdx = length(fieldnames(X));
dfdx = zeros(sizeDfdx);

% derivative w.r.t. muFace
if u.iTrial~=1
    for iFace=1:10
        dfdx(iFace,iFace)=1;
    end
    [dfdx(u.identityFace,u.identityFace),dfdx(u.identityFace,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    dfdx(u.identityFace,end-5:end) = dVarPxWrtFace(muFace,sigmaFace,GMU,JP,inF,0,X,temperatureConfidence,grid,varConfidence);
end

% w.r.t sigmaFace
if u.iTrial~=1
    [dfdx(u.identityFace+5,u.identityFace),dfdx(u.identityFace+5,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    dfdx(u.identityFace+5,end-5:end) = dVarPxWrtFace(muFace,sigmaFace,GMU,JP,inF,1,X,temperatureConfidence,grid,varConfidence);
end

% w.r.t. muTarget
dfdx(11,11)=1;
[dfdx(11,u.identityFace), dfdx(11,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,0);
dfdx(11,end-5:end) = dVarPxWrtTarget(X,GX,JP,inF,0,temperatureConfidence,grid,varConfidence);

% w.r.t. sigmaTarget
dfdx(12,12)=1;
[dfdx(12,u.identityFace), dfdx(12,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,1);
dfdx(12,end-5:end) = dVarPxWrtTarget(X,GX,JP,inF,1,temperatureConfidence,grid,varConfidence);

%% dfdp
sizeDfdp = length(fieldnames(theta));
dfdp = zeros(sizeDfdp,sizeDfdx);

% w.r.t. muFace_0
if u.iTrial == 1
    dfdp(1,1:5) = 1;
    [dfdp(1,u.identityFace), dfdp(1,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    dfdp(1,end-5:end) = dVarPxWrtFace(muFace,sigmaFace,GMU,JP,inF,0,X,temperatureConfidence,grid,varConfidence);
end
    
% w.r.t. sigmaFace_0
if u.iTrial == 1
    dfdp(2,6:10) = 1;
    [dfdp(2,u.identityFace), dfdp(2,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    dfdp(2,end-5:end) = dVarPxWrtFace(muFace,sigmaFace,GMU,JP,inF,1,X,temperatureConfidence,grid,varConfidence);
end

% w.r.t. sigmaAdvice 
[dfdp(3,u.identityFace), dfdp(3,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdSigma,grid);
dfdp(3,end-5:end) = dVarPxWrtAdvice(JP,dJPdSigma,inF,X,temperatureConfidence,grid,varConfidence);

% w.r.t. sigmaFacePercept
[dfdp(4,u.identityFace), dfdp(4,u.identityFace+5)] = derivatePmuWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,grid,1);
dfdp(4,end-5:end) = dVarPxWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,inF,1,X,temperatureConfidence,grid,varConfidence);

% % w.r.t. slope (inverse temperature) of advice
[dfdp(5,u.identityFace), dfdp(5,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdTemperatureAdvice,grid);
dfdp(5,end-5:end) =  dVarPxWrtAdvice(JP,dJPdTemperatureAdvice,inF,X,temperatureConfidence,grid,varConfidence);

% w.r.t. gamma (intentional variability)
[dfdp(6,u.identityFace), dfdp(6,u.identityFace+5)] = derivatePmuWrtFace(GMU,gamma,GE,JP,grid,1);
dfdp(6,end-5:end) = dVarPxWrtFace(GMU,gamma,GE,JP,inF,1,X,temperatureConfidence,grid,varConfidence);

% wrt confidence temperature
dfdp(7,end-5) = -X.confidence*(1-X.confidence)*(varConfidence-shiftConfidence)/temperatureConfidence;
dfdp(8,end-5) = -X.confidence*(1-X.confidence)/temperatureConfidence;

end

function [derivatives] = dVarPxWrtFace(muFace,sigmaFace,GMU,JP,inF,isSigma,X,temperatureConfidence,grid,varConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace)./sigmaFace^2),3),2));
    elseif isSigma==1
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace))/sigmaFace^3,3),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dVarConfidence = (grid.gx.^2 * dNormalizedPx - 2 * (grid.gx*doubleSumJP / tripleSumJP)*(grid.gx*dNormalizedPx))...
        / (2 * sqrt((grid.gx).^2 * (doubleSumJP / tripleSumJP) - (grid.gx * (doubleSumJP / tripleSumJP)).^2));
    dSigmoidConfidence = -dVarConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end

function [derivatives] = dVarPxWrtTarget(X,GX,JP,inF,isSigma,temperatureConfidence,grid,varConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    if isSigma==0 % then we derivate wrt mean
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GX-X.muTarget)./(exp(X.sigmaTarget))^2),3),2));
    elseif isSigma==1 % then we derivate wrt sigma
        dDoubleSumJP=VBA_vec(sum(sum(JP*exp(X.sigmaTarget).*(GX-X.muTarget-exp(X.sigmaTarget)).*(GX-X.muTarget+exp(X.sigmaTarget))./exp(3*X.sigmaTarget),3),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dVarConfidence = (grid.gx.^2 * dNormalizedPx - 2 * (grid.gx*doubleSumJP / tripleSumJP)*(grid.gx*dNormalizedPx))...
        / (2 * sqrt((grid.gx).^2 * (doubleSumJP / tripleSumJP) - (grid.gx * (doubleSumJP / tripleSumJP)).^2));
    dSigmoidConfidence = -dVarConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
    
    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end

function [derivatives] = dVarPxWrtAdvice(JP,dJPdTheta,inF,X,temperatureConfidence,grid,varConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP, 3),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(dJPdTheta, 3),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dVarConfidence = (grid.gx.^2 * dNormalizedPx - 2 * (grid.gx*doubleSumJP / tripleSumJP)*(grid.gx*dNormalizedPx))...
        / (2 * sqrt((grid.gx).^2 * (doubleSumJP / tripleSumJP) - (grid.gx * (doubleSumJP / tripleSumJP)).^2));
    dSigmoidConfidence = -dVarConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
    
    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end


