function [dgdx,dgdp] = ketabi_g_derivative_v0020(gx,inG,matX,temperatureShoot,isSoftmaxPx)

%% dgdx
sizeDgdx = [length(matX) length(gx)];
dgdx = zeros(sizeDgdx);

% bins
bin = matX((end-inG.nBin+1):end);
if isSoftmaxPx == 1
    dgdx(end-inG.nBin+1:end,:) = -gx(1:end)*gx(1:end)'/temperatureShoot;
    for i = 1:inG.nBin  
        dgdx(i+end-inG.nBin,i) = gx(i)*(1-gx(i))/temperatureShoot;
    end
elseif isSoftmaxPx == 0
    dgdx(end-inG.nBin+1:end,:) = eye(5);
end

%% dgdp
sizeDgdp = [1 length(gx)];
dgdp = zeros(sizeDgdp);
if isSoftmaxPx == 1
    for iBin = 1:inG.nBin
        u = exp(bin(iBin)/temperatureShoot);
        v = sum(exp(bin/temperatureShoot));
        dUdTemperature = -bin(iBin)*exp(bin(iBin)/temperatureShoot)/temperatureShoot^2;
        dVdTemperature = -sum(bin.*exp(bin/temperatureShoot))/temperatureShoot^2;
        dgdp(iBin) = (dUdTemperature * v - u * dVdTemperature) / v^2 * temperatureShoot;   % multiply by temperatureShoot as it is elevated to the exponential in @g
    end
elseif isSoftmaxPx == 0
    dgdp = 0;
end

end

