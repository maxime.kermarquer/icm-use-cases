function [dfdx,dfdp] = ketabi_f_derivative_v0010(muFace,sigmaFace, X, theta, u, grid, inF, GX, GE, GMU, JP, dJPdSigma)


currentFace = inF.listFaceIdentity{u.identityFace};
muFace_0 = theta.muFace_0;
sigmaFace_0 = exp(theta.sigmaFace_0);
sigmaAdvice = exp(theta.sigmaAdvice);
sigmaFace_percept = exp(theta.sigmaFace_percept);
temperatureAdvice = exp(theta.temperatureAdvice);
gamma = exp(theta.gamma);

%% dfdx
sizeDfdx = length(fieldnames(X));
dfdx = zeros(sizeDfdx);

% derivative w.r.t. muFace
if u.iTrial~=1
    for iFace=1:10
        dfdx(iFace,iFace)=1;
    end
    [dfdx(u.identityFace,u.identityFace),dfdx(u.identityFace,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    derivatives = derivatePxWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,0);
    dfdx(u.identityFace,end-4:end) = derivatives(2:end);
end

% w.r.t sigmaFace
if u.iTrial~=1
    [dfdx(u.identityFace+5,u.identityFace),dfdx(u.identityFace+5,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    derivatives = derivatePxWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,1);
    dfdx(u.identityFace+4,end-4:end) = derivatives(2:end);
end

% w.r.t. muTarget
dfdx(11,11)=1;
[dfdx(11,u.identityFace), dfdx(11,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,0);
derivatives = derivatePxWrtTarget(X,GX,JP,inF,grid,0);
dfdx(11,end-4:end) = derivatives(2:end);

% w.r.t. sigmaTarget
dfdx(12,12)=1;
[dfdx(12,u.identityFace), dfdx(12,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,1);
derivatives = derivatePxWrtTarget(X,GX,JP,inF,grid,1);
dfdx(12,end-4:end) = derivatives(2:end);

%% dfdp
sizeDfdp = length(fieldnames(theta));
dfdp = zeros(sizeDfdp,sizeDfdx);

% w.r.t. muFace_0
if u.iTrial == 1
    dfdp(1,1:5) = 1;
    [dfdp(1,u.identityFace), dfdp(1,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    derivatives = derivatePxWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,0);
    dfdp(1,end-4:end) = derivatives(2:end);
end
    
% w.r.t. sigmaFace_0
if u.iTrial == 1
    dfdp(2,6:10) = 1;
    [dfdp(2,u.identityFace), dfdp(2,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    derivatives = derivatePxWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,1);
    dfdp(2,end-4:end) = derivatives(2:end);
end

% w.r.t. sigmaAdvice 
% dLLexydSigma = derivativeLJwrtSigmaAdvice(GX,GE,u.advicePosition,grid);
% dJPdSigma = dLLexydSigma.*JP;
[dfdp(3,u.identityFace), dfdp(3,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdSigma,grid);
derivatives = derivatePxWrtAdvice(JP,dJPdSigma,grid,inF);
dfdp(3,end-4:end)  = derivatives(2:end);

% w.r.t. sigmaFacePercept
[dfdp(4,u.identityFace), dfdp(4,u.identityFace+5)] = derivatePmuWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,grid,1);
derivatives = derivatePxWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,inF,grid,1);
dfdp(4,end-4:end) = derivatives(2:end);

% % w.r.t. slope (inverse temperature) of advice
% dedSlope = grid.ge .* (1-1 / (1 + exp(-grid.ge*round(grid.slope)))) .* (1 / (1 + exp(-grid.ge*round(grid.slope))));
% dpydSlope = 
% dJPdSlope = JP .* grid.ge;

% w.r.t. gamma (intentional variability)
[dfdp(6,u.identityFace), dfdp(6,u.identityFace+5)] = derivatePmuWrtFace(GMU,gamma,GE,JP,grid,1);
derivatives = derivatePxWrtFace(GMU,gamma,GE,JP,inF,grid,1);
dfdp(6,end-4:end) = derivatives(2:end);

end

