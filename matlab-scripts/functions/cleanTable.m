function [ myNewTable ] = cleanTable( myTable, NaN_number )
% [ myNewTable ] = cleanTable( myTable, NaN_number )
% Clean table such that non numeric values are set to NaN
% values equal to NaN_number are set to NaN as well

if nargin == 1
    NaN_number = 99999;
end

myNewTable = myTable;

[nL, nC] = size(myTable);

cName = myTable.Properties.VariableNames;
for iC = 1:nC
    temp = myTable.(cName{iC});
    if iscell(temp)
        temp(cellfun(@isnumeric, temp))  = cellfun(@num2str, temp(cellfun(@isnumeric, temp)), 'UniformOutput', 0);
        [temp,isGood]=cellfun(@str2num, temp, 'UniformOutput', 0);
        isGood = cell2mat(isGood);
        aga = NaN(nL, 1);
        aga(isGood == 1) = cell2mat(temp);
        myNewTable.(cName{iC}) = aga;
    end
    
    myNewTable.(cName{iC})(ismember(myNewTable.(cName{iC}), NaN_number),:) = NaN;
    
end



end

