function [derivatives] = derivatePxWrtTemperatureAdvice(JP,dJPdTemperatureAdvice,grid,inF)

    doubleSumJP = VBA_vec(sum(sum(JP, 3),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(dJPdTemperatureAdvice, 3),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential

    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    dConfidencedMuTarget = (grid.gx.^2 * dNormalizedPx - 2 * (grid.gx*doubleSumJP / tripleSumJP)*(grid.gx*dNormalizedPx))...
        / (2 * sqrt((grid.gx).^2 * (doubleSumJP / tripleSumJP) - (grid.gx * (doubleSumJP / tripleSumJP)).^2));
    derivatives=[dConfidencedMuTarget dBin];

end

