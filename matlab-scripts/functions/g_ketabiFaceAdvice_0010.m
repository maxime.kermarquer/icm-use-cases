function [gx,dgdx,dgdp] = g_ketabiFaceAdvice_0010(x_t,P,u_t,inG)
%%function [gx] = g_ketabiFaceAdvice_0009(x_t,P,u_t,inG)
%
% INPUT
% - x_t : 
%           muFace_(faceIdentity)     = mean of the prior distribution for faceIdentity
%           sigmaFace_(faceIdentity)  = variance of the prior distribution for faceIdentity
%           muTarget                  = mean of the prior distribution for target position
%           sigmaTarget               = variance of the prior distribution for target position
%           bin(i)                    = posterior distribution of target position divided in nBin bins
% - P : 
%           temperatureShoot          = temperature of the dependance between choice of shoot position and probability of success
% - u_t :
%           advicePosition       
%           identityFace     
%           degreeExpression       
%           iTrial                      
% - inG :
%           listFaceIdentity : a cell with all face identitity in the correct order (faceNumber)
%           nBin : number of bins to divide the posterior distribution of the target position
%           isConfidence : is confidence fit (1) or not(0)
%           isSoftmaxPx : use softmax to choose the best shooting position according to the probability of success if (1), use raw probability if (0)         
% OUTPUT
% - gx :   both moments of the posterior

%% Get parameters and u (experimental conditions)
%--------------------------------------------------------------------------
[X, phi,u] = getStateParamInput(x_t,P, u_t,inG);
temperatureShoot = exp(phi.temperatureShoot);

% shooting position information
matX = table2array(struct2table(X));
isSoftmaxPx = 1;
if  isSoftmaxPx == 0
    gx = matX((end-inG.nBin+1):end)';
elseif isSoftmaxPx == 1
    gx = exp(matX((end-inG.nBin+1):end)' / temperatureShoot) ./ sum(exp(matX((end-inG.nBin+1):end)' / temperatureShoot));
end

[dgdx,dgdp] = ketabi_g_derivative_v0010(gx,inG,matX,temperatureShoot,isSoftmaxPx);

end

