function [ newV ] = changeLabel_order( V, newLabel, oldLabel )
%% This function is a home-made version of renamecats that changes the values of a vector / factor

V = nominal(V);
label = unique(V);
if nargin == 3
    oldLabel = nominal(oldLabel);
else
    oldLabel = label;
end
newLabel=nominal(newLabel);
nOld = length(oldLabel);
nNew = length(newLabel);


for iOld = 1:nOld
    newV(V==oldLabel(iOld)) = newLabel(iOld);
end


end

