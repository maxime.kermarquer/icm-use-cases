function [dMuFacedMuTarget,dSigmaFacedMuTarget] = derivatePmuWrtTarget(X,GX,JP,grid,isSigma)

    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((GX-X.muTarget)./(exp(X.sigmaTarget))^2),1),2));
        dTripleSumJP = sum(dDoubleSumJP);
    elseif isSigma==1
        dDoubleSumJP=VBA_vec(sum(sum(JP.*((GX-X.muTarget-exp(X.sigmaTarget)).*(GX-X.muTarget+exp(X.sigmaTarget))./(exp(2*X.sigmaTarget))),1),2));
        dTripleSumJP = sum(dDoubleSumJP);
    end
    dNormalizedPmu = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    dMuFacedMuTarget = grid.gmu * dNormalizedPmu;
    dSigmaFacedMuTarget = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
        / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));
end

