function [LJ,dLLexydSigma] = logJointFlat(x,e,mu,LP0mu,y,f,grid)
% log joint distribution: log p(x,e,mu,y,f)
% function LJ = logJoint(x,e,mu,LP0mu,y,f,P)
% IN:
%   - x: the values of x at which log p(x,e,mu,y,f) is evaluated
%   - e: the values of e at which log p(x,e,mu,y,f) is evaluated
%   - mu: the values of mu at which log p(x,e,mu,y,f) is evaluated
%   - LP0mu: the log prior distribution on mu, evaluated on the grid P.gmu
%   - y: the values of y at which log p(x,e,mu,y,f) is evaluated
%   - f: the values of f at which log p(x,e,mu,y,f) is evaluated
%   - P: parameter structure
% OUT:
%   - LJ: the log joint distribution: log p(x,e,mu|y,f)

[LLexy,dLLexydSigma]...
                = LLexFlat(e,x,y,grid); % log p(y|x,e) & d(log(py))dSigma & d(log(py))dTemperatureAdvice
LPx             = logGauss(x,grid.muTarget,grid.sigmaTarget,grid.d.x); % log p(x)
LPemu           = logGauss(e,mu,grid.gamma,grid.d.e); % log p(e|mu)
LLef            = logGauss(f,e,grid.ohmega,grid.d.f); % log p(f|e)
LJ              = LLexy + LPx + LPemu + LLef + LP0mu; % log p(x,e,mu,y,f)
