function [ dKL ] = discretedKL( P, Q )
%Compute Kullback?Leibler divergence for discrete distribution P and Q
% When  P(i)=Q(i) = 0; we assume p*log(p/q) = 0
% When  P(i) = 0 and  Q(i)  ~0, P we use P(i) = eps (same for Q)


isPnull = P==0;
isQnull = Q==0;

bothNull = isPnull & isQnull;

%assert(all(isPnull == isQnull), 'No absolute continuity');

% Approximation
P(isPnull==1)=eps;
Q(isQnull==1)=eps;

dKL = arrayfun(@(p, q) p*log(p/q), P, Q);
dKL(bothNull==1)=0;
dKL = sum(dKL);

end

