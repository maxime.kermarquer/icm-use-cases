function [ sem ] = sem(x, dim)
%% [ sem ] = sem(x, dim)
% Compute the standard error of the mean
if nargin == 1
    if ndims(x) == 2
        temp = size(x);
        dim = find(temp == max(temp));
    else
        dim = 1;
    end
end
sem = nanstd(x, [], dim) ./ sqrt(sum(~isnan(x), dim));


end

