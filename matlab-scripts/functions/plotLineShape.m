function [ lineOption, patchOption ] = plotLineShape (x, mu, phi, color, alpha, lineWidth)
%% This is a wrapper to plot a line + error bar using shape

try 
    assert(~isempty(color));
catch
    color = [0 0 0];
end


try 
    assert(~isempty(alpha));
catch
    alpha = 0.2;
end
    

try 
    assert(~isempty(lineWidth));
catch
    lineWidth = 3;
end


xx = [x x(end:-1:1)];
mm = [mu mu(end:-1:1)];
pp = [-phi phi(end:-1:1)];
yy=mm+pp;

lineOption = plot(x, mu, 'color', color, 'LineWidth', lineWidth);
patchOption =patch(xx, yy, color, 'FaceAlpha', alpha, 'LineStyle', 'none');
set(gca, 'LineWidth', 2)
set(gca, 'box', 'on')
set(gcf, 'color', 'white')


end

