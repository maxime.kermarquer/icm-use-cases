%% Runs model-based analyses for ketabi face advice experiment for one participant (i)
% v0026 : Evolution through the computation of the joint probaility p(x, e, mu | yx, ye)
% fit of position and confidence, bins of equal number of y hits, confidence as sigmoid(posterior at shoot position)
% with intertrial noise
% in v26 : gamma is removed, sigmaPercept value is used to link disposition and intention instead

%% Get version
ketabiAdviceVersion = 'v0026';

%% Get dir
% if isfolder('/Users/fabienvinckier/ownCloud')
%     homedir   = '/Users/fabienvinckier/ownCloud/teamVinckier/ketabiFaceAdvice';
%     datadir   = fullfile(homedir, 'data', 'control_prisme');
%     resultdir = fullfile(homedir, 'result', 'ketabiFace', ketabiAdviceVersion);
%     scriptdir = fullfile(homedir, 'script_anl', 'MB_analysis');
%     addpath(scriptdir);
% elseif isfolder('C:\Users\jacob.dhote\ownCloud')
%     homedir   = 'C:\Users\jacob.dhote\ownCloud\teamVinckier\ketabiFaceAdvice';
%     datadir = fullfile(homedir, 'data', 'control_prisme');
%     resultdir = fullfile(homedir, 'result', 'ketabiFace', ketabiAdviceVersion);
%     scriptdir = fullfile(homedir, 'script_anl', 'MB_analysis');
%     addpath(genpath(scriptdir));
% elseif isfolder('C:\Users\jac_d\ownCloud')
%     homedir   = 'C:\Users\jac_d\ownCloud\teamVinckier\ketabiFaceAdvice';
%     datadir = fullfile(homedir, 'data', 'control_prisme');
%     resultdir = fullfile(homedir, 'result', 'ketabiFace', ketabiAdviceVersion);
%     scriptdir = fullfile(homedir, 'script_anl', 'MB_analysis');
%     addpath(genpath(scriptdir));
% elseif isfolder('/cluster')
%     homedir   = '/cluster';
%     datadir   = fullfile(homedir, 'home', 'vfabien', 'ketabi', 'data', 'control_prisme');
%     resultdir = fullfile(homedir, 'scratch', 'vfabien','ketabi', ketabiAdviceVersion);
% elseif isfolder('\\lexport')
    homedir   = [filesep 'network' filesep 'lustre' filesep 'iss01' filesep 'mbb' filesep 'raw' filesep 'jacob.dhote' filesep 'cluster' filesep];
%     homedir = '\\lexport\iss01.mbb\raw\jacob.dhote\cluster\';
    datadir   = [homedir, 'datadir'];
    resultdir = [homedir, 'resultdir'];
    scriptdir = [homedir, 'scriptdir'];
    addpath(datadir);
    addpath(genpath(scriptdir));
% end

%% Get the indexation of subjects
listFile = dir([datadir filesep 'ketabiFaceAdvice_*session_1_*']);
nFile = length(listFile);

parfor iFile = 1:nFile
    fileName = listFile(iFile).name;
    data = getfield(load([datadir filesep fileName]),'data');
    data.tableData.confidence = cellfun(@mean, {data.trial.confidence}'); 
    nT = height(data.tableData); % change to reduce the number of trials
    data.tableData = data.tableData(1:nT,:); % Reduced number of T (demo version)

    subjectMatlabNumber = str2double(regexp(fileName, '\d{3}', 'match', 'once'));
    groupName = 'prisme_HC';
    tableResultFull = table;
    tableResultFull.subjectMatlabNumber = repmat(subjectMatlabNumber, 1);
    tableResultFull.iFile = iFile;
%     tableResultFull.reversal = data.iReversal;
    tableResultFull.model = {'position and confidence, as posterior at shoot position'};
    if sum(ismember(data.tableData.faceIdentity{1}, 'L')) == 0
        stimulus = 'face';
    elseif sum(ismember(data.tableData.faceIdentity{1}, 'L')) == 1
        stimulus = 'lightbulb';
    end

    %% Define function
    f_fname = @f_ketabiFaceAdvice_0026;   % Evolution function   (that actually does all the job)
    g_fname = @g_ketabiFaceAdvice_0026;   % Observarion function (it just reads the correct input)

    %% Run
    options=struct;
    dim = struct('n',0, 'n_theta', 0, 'n_phi', 0);

    %% Define y, isYOut (remove outlier values)

    nBin = 10;
    y = zeros(nBin + 1, length(data.tableData.confidence));
    temp = NaN(length(data.tableData.confidence) / nBin , nBin);
    
    % 1st source : confidence
    y(1, :) = data.tableData.confidence';
    
    % 2nd source : shooting position
    rangeShootingPosition = -11:63;
    %y(1:nBin, length(data.tableData.finalPosition)) = 0;
    
    sortedData = sort(data.tableData.finalPosition);
    temp = reshape(sortedData, [length(data.tableData.confidence) / nBin , nBin]);
    minBin = temp(1,:);
    binShooting = sum(data.tableData.finalPosition > minBin, 2);
    binShooting(binShooting == 0) = 1;
    for iTrial = 1:length(binShooting)
        y(binShooting(iTrial)+1, iTrial) = 1;
    end
    
    options.isYout = isnan(y);
    y(options.isYout)=0;
    
    nTrial = size(y, 2);
    dim.nT=nTrial;
    options.skipf = zeros(1, nTrial);     % Skip evolution fonction for these trial
    dim.p = size(y, 1);
    
    options.extended=1;
    options.sources(1).out  = 1;
    options.sources(1).type = 0;
    options.sources(2).out  = 2:nBin + 1;
    options.sources(2).type = 1;

    %% Define prior for hidden states
    % Mu & sigma for each face
    temp = varfun(@mean, data.tableData, 'GroupingVariables', {'faceNumber', 'faceIdentity', 'faceContingency'}, 'InputVariable', {'finalPosition'});
    listBin = {'bin1', 'bin2', 'bin3', 'bin4', 'bin5', 'bin6', 'bin7', 'bin8', 'bin9', 'bin10'};
    listX = [strcat('muFace_', temp.faceIdentity) ; strcat('sigmaFace_', temp.faceIdentity) ; 'confidence' ; listBin']';
    muX0 =   [zeros(1, 5), zeros(1, 5), 0, zeros(1,nBin)]'  ;
    sigmaX0 = [zeros(1, 5), zeros(1, 5), 0, zeros(1,nBin)];
    
    [dim, options] = setListPriors(options, dim, 'X0', listX, muX0, sigmaX0);

    %% Define priors for evolution parameters
    [dim, options] = setPriors(options, dim, 'theta',...
        'muFace_0',                     0, 1,...            % mean of prior on the intention of people in general
        'sigmaFace_0',                  log(1), 1,...       % std of prior on the intention of people in general
        'muTarget',                     26, 0,...           % mean of prior of target position (fixed in this version of the model)
        'sigmaTarget',                  log(15), 1,...      % std of prior of target position
        'sigmaAdvice',                  log(4.8), 1,...     % precision of the advice(range where the advice gives information)
        'sigmaFace_percept',            log(1), 1,...       % precision of the emotion depending of the facial expression
        'temperatureAdvice',            log(10), 1,...      % inverse temperature of the sigmoid used to transform e into b
        'interTrialNoise',              log(.01), 5,...     % noise to allow sigmaFace to change from 1 trial to another
        'temperatureConfidence',        log(.05), 1, ...     % temperature of the dependance between confidence and probability of success
        'shiftConfidence',              0, 1);              % shift of the dependance between confidence and probability of success

    %% Define priors for observation parameters
    [dim, options] = setPriors(options, dim, 'phi',...        
        'temperatureShoot',             log(.1), 1);        % temperature of the dependance between choice of shoot position and probability of success

    %% Define options (in)
    options.inF.listFaceIdentity     = temp.faceIdentity;
    options.inF.nBin                 = nBin;
    options.inF.minBin               = minBin;    
%     options.inF.thetaLabel           = char(fieldnames(options.inF.thetaLabel));
    options.inG.nBin                 = nBin;
%     options.inG.phiLabel             = char(fieldnames(options.inG.phiLabel));
    
    % State noise precision (only for dynamical systems)
    options.priors.a_alpha = Inf ;
    options.priors.b_alpha = 0 ;

    % Define hyperpriors
    [options.priors.a_sigma(1),options.priors.b_sigma(1)] = VBA_guessHyperpriors(y(1, options.isYout(1,:)==0)) ;

    % Get u
    data.tableData.degreeExpression(abs(data.tableData.degreeExpression)==1) = 0;
    data.tableData.degreeExpressionPercept = str2num(char(changeLabel(data.tableData.degreeExpression, [-10 -4 -1 0 1 4 10],  unique(data.tableData.degreeExpression))));

    [u, dim, options]=setInput(options,dim,...
        'advicePosition',       data.tableData.advicePosition',...            % advice position
        'identityFace',         data.tableData.identityFace',...              % face identity
        'degreeExpression',     data.tableData.degreeExpressionPercept', ...  % face expression (from -100% to 100% according to perception)
        'iTrial',               1:nTrial, ...                                 % iTrial
        'finalPosition',        data.tableData.finalPosition');                                  

    %% Define options and misc
    options.checkGrads      = 0;
    options.DisplayWin      = 0; % Do you want to display fig during inversion?
    options.GnFigs          = 0;
    options.dim             = dim;
    options.verbose         = 1; % Do you want to display text during inversion ?

    %% Do inversion
    [posterior,out] = VBA_NLStateSpaceModel(y,u,f_fname,g_fname,dim,options);
    if isempty(out)
        error('out is empty : please check');
    end

    %% Get free energy
%     tableResultFull.F(iModel, 1) = out.F;
    %         %% Get fitted free parameters
    %         [muX,muTheta,muPhi] = getStateParamInput(posterior.muX,posterior.muTheta, u,options.inF);
    %         listVar = fieldnames(options.inF.thetaLabel);
    %         for iVar = 1:length(listVar)
    %             tableResultFull.([listVar{iVar}])(iModel, 1) = muTheta.(listVar{iVar});
    %         end
    %



    %         %% Get fitted hidden states
    %         listVar = {'optimalResource', 'normalizedPeakForce', 'netValue', 'peakForce', 'yank'};
    %         for iVar = 1:length(listVar)
    %             tableResultFull.(['fitModel_' listVar{iVar}])(iModel, 1)  = {muX.(listVar{iVar})'};
    %         end

    %% Store full results
    tableResultFull.posterior = posterior;
    tableResultFull.out = out;

    tableResultSummary = tableResultFull;
    tableResultSummary.posterior = [];
    tableResultSummary.out = [];
    summaryName = [resultdir filesep ketabiAdviceVersion '_' groupName '_' num2str(subjectMatlabNumber) '_' stimulus '_summary_' strrep(strrep(datestr(clock), ' ', '-'), ':', '-') '.mat'];
    saveToFile(summaryName, tableResultSummary);
    resultName  = [resultdir filesep ketabiAdviceVersion '_' groupName '_' num2str(subjectMatlabNumber) '_' stimulus '_full_' strrep(strrep(datestr(clock), ' ', '-'), ':', '-') '.mat'];
    saveToFile(resultName, tableResultFull);
end