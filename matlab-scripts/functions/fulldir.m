function [ listFile ] = fulldir( reg_exp )
%% This is just a wrapper to get directly the (full path) list of files in a directory.
% function [ listFile ] = fulldir( reg_exp )
% 


temp = decomposeName(reg_exp, filesep);
temp = temp(~cellfun(@isempty, temp));
if length(temp) == 1
    myPath = pwd;
else
    myPath = fullfile(temp{1:(end-1)});
end

if reg_exp(1) == filesep
    myPath = [filesep myPath];
end
myPath = what(myPath);
myPath = myPath.path;

myFile = dir(reg_exp);
myFile = {myFile(:).name}';

listFile = fullfile(myPath, myFile);

end
 