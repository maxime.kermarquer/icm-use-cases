%% This is for editing an str expression in a script or several scripts
% To sum up:
% 1. we create a .txt from the .m and a str from the .txt
% 2. we replace expression1 with expression2 in the str
% 3. we write the str in the .txt
% 4. we create a .m from the .txt
% 5. the original .m and the .txt are deleted

nFile=72;

for i=1:nFile
    % define file and expressions
    fileName = ['ketabiFaceAdvice_v0009_subject_' sprintf('%d',i)];
    expression1 = 'parfor iFile = 1:24';
    expression2 =  ['parfor iFile = ' sprintf('%d',i)]; 
    
    % edit
    copyfile([fileName '.m'], [fileName '.txt'])
    aga=fileread([fileName '.txt']);
    aga=regexprep(aga,expression1, expression2);
    fid=fopen([fileName '.txt'],'w');
    fwrite(fid,aga);
    fclose(fid);
    
    % save and delete
    delete([fileName '.m']);
    copyfile([fileName '.txt'], [fileName '.m'])
    delete([fileName '.txt']);
end

