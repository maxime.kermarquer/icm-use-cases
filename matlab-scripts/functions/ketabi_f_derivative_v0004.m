function [dfdx,dfdp] = ketabi_f_derivative_v0004(muFace,sigmaFace, X, theta, u, grid, inF, GX, GE, GMU, JP, dJPdSigma, dJPdTemperatureAdvice, shootPosition, shootConfidence)


currentFace = inF.listFaceIdentity{u.identityFace};
muFace_0 = theta.muFace_0;
sigmaFace_0 = exp(theta.sigmaFace_0);
sigmaAdvice = exp(theta.sigmaAdvice);
sigmaFace_percept = exp(theta.sigmaFace_percept);
temperatureAdvice = exp(theta.temperatureAdvice);
gamma = exp(theta.gamma);
temperatureConfidence = exp(theta.temperatureConfidence);
shiftConfidence = theta.shiftConfidence;

%% dfdx
sizeDfdx = length(fieldnames(X));
dfdx = zeros(sizeDfdx);

% derivative w.r.t. muFace
if u.iTrial~=1
    for iFace=1:10
        dfdx(iFace,iFace)=1;
    end
    [dfdx(u.identityFace,u.identityFace),dfdx(u.identityFace,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    dSigmoidSigmoidConfidence = dShootConfidenceWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,0,shootPosition);
    dfdx(u.identityFace,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
end

% w.r.t sigmaFace
if u.iTrial~=1
    [dfdx(u.identityFace+5,u.identityFace),dfdx(u.identityFace+5,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    dSigmoidSigmoidConfidence = dShootConfidenceWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,1,shootPosition);
    dfdx(u.identityFace+5,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
end

% w.r.t. muTarget
dfdx(11,11)=1;
[dfdx(11,u.identityFace), dfdx(11,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,0);
dSigmoidSigmoidConfidence = dShootConfidenceWrtTarget(X,GX,JP,inF,grid,0,shootPosition);
dfdx(11,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
   
% w.r.t. sigmaTarget
dfdx(12,12)=1;
[dfdx(12,u.identityFace), dfdx(12,u.identityFace+5)] = derivatePmuWrtTarget(X,GX,JP,grid,1);
dSigmoidSigmoidConfidence = dShootConfidenceWrtTarget(X,GX,JP,inF,grid,1,shootPosition);
dfdx(12,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

%% dfdp
sizeDfdp = length(fieldnames(theta));
dfdp = zeros(sizeDfdp,sizeDfdx);

% w.r.t. muFace_0
if u.iTrial == 1
    dfdp(1,1:5) = 1;
    [dfdp(1,u.identityFace), dfdp(1,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,0);
    dSigmoidSigmoidConfidence = dShootConfidenceWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,0,shootPosition);
    dfdp(1,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
end
    
% w.r.t. sigmaFace_0
if u.iTrial == 1
    dfdp(2,6:10) = 1;
    [dfdp(2,u.identityFace), dfdp(2,u.identityFace+5)] = derivatePmuWrtFace(muFace,sigmaFace,GMU,JP,grid,1);
    dSigmoidSigmoidConfidence = dShootConfidenceWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,1,shootPosition);
    dfdp(2,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
end

% w.r.t. sigmaAdvice 
% dLLexydSigma = derivativeLJwrtSigmaAdvice(GX,GE,u.advicePosition,grid);
% dJPdSigma = dLLexydSigma.*JP;
[dfdp(3,u.identityFace), dfdp(3,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdSigma,grid);
dSigmoidSigmoidConfidence = dShootConfidenceWrtAdvice(JP,dJPdSigma,grid,inF,shootPosition);
dfdp(3,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

% w.r.t. sigmaFacePercept
[dfdp(4,u.identityFace), dfdp(4,u.identityFace+5)] = derivatePmuWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,grid,1);
dSigmoidSigmoidConfidence = dShootConfidenceWrtFace(u.degreeExpression,sigmaFace_percept,GE,JP,inF,grid,1,shootPosition);
dfdp(4,end) =  dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

% w.r.t. slope (inverse temperature) of advice
[dfdp(5,u.identityFace), dfdp(5,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdTemperatureAdvice,grid);
dSigmoidSigmoidConfidence = dShootConfidenceWrtAdvice(JP,dJPdTemperatureAdvice,grid,inF,shootPosition);
dfdp(5,end) =  dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

% w.r.t. gamma (intentional variability)
[dfdp(6,u.identityFace), dfdp(6,u.identityFace+5)] = derivatePmuWrtFace(GMU,gamma,GE,JP,grid,1);
dSigmoidSigmoidConfidence = dShootConfidenceWrtFace(GMU,gamma,GE,JP,inF,grid,1,shootPosition);
dfdp(6,end) = dSigmoidSigmoidConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

% wrt confidence temperature
dfdp(7,end) = -X.confidence*(1-X.confidence)*(shootConfidence-shiftConfidence)/temperatureConfidence;
dfdp(8,end) = -X.confidence*(1-X.confidence)/temperatureConfidence;

end

function [dShootConfidence] = dShootConfidenceWrtFace(muFace,sigmaFace,GMU,JP,inF,grid,isSigma,shootPosition)

doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
tripleSumJP = sum(doubleSumJP); % this is normalization constant for px

if isSigma==0
    dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace)./sigmaFace^2),3),2));
elseif isSigma==1
    dDoubleSumJP = VBA_vec(sum(sum(JP.*((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace))/sigmaFace^3,3),2));
end
dTripleSumJP = sum(dDoubleSumJP);
dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;
dShootConfidence = dNormalizedPx(round(shootPosition));

end

function [dShootConfidence] = dShootConfidenceWrtTarget(X,GX,JP,inF,grid,isSigma,shootPosition)

doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
tripleSumJP = sum(doubleSumJP); % this is normalization constant for px

if isSigma==0 % then we derivate wrt mean
    dDoubleSumJP = VBA_vec(sum(sum(JP.*((GX-X.muTarget)./(exp(X.sigmaTarget))^2),3),2));
elseif isSigma==1 % then we derivate wrt sigma
    dDoubleSumJP=VBA_vec(sum(sum(JP*exp(X.sigmaTarget).*(GX-X.muTarget-exp(X.sigmaTarget)).*(GX-X.muTarget+exp(X.sigmaTarget))./exp(3*X.sigmaTarget),3),2));
end
dTripleSumJP = sum(dDoubleSumJP);
dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;
dShootConfidence = dNormalizedPx(round(shootPosition));

end

function [dShootConfidence] = dShootConfidenceWrtAdvice(JP,dJPdTheta,grid,inF,shootPosition)

doubleSumJP = VBA_vec(sum(sum(JP, 3),2)); % this is pmu
tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
dDoubleSumJP = VBA_vec(sum(sum(dJPdTheta, 3),2));
dTripleSumJP = sum(dDoubleSumJP);
dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;
dShootConfidence = dNormalizedPx(round(shootPosition));

end

