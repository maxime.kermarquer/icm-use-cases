function [ varargout ] = selectOutput( func, iOutput, varargin)
%% function [ varargout ] = selectOutput( func, iOutput, varargin)
%% This is a dummy function to select one or more outputs of a function
%  i.e.  varargout = func(varargin)(iOutput)
varargout = cell(1,nargout(func));
[varargout{:}] = func(varargin{:});
varargout = varargout(iOutput);
end

