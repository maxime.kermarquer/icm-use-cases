
% fonction d'evolution : advicePosition & priorTarget -> posteriorTarget pour visage neutre (50/50 negatif/positif)
f_fname = @f_ketabi;

% fonction d'observation : posteriorTarget -> finalPosition
g_fname = @g_ketabi;
    
% parametres de simulation
n_t = 400;
theta = 0;
phi = 1;
x0 = priorTarget;

% dimensions
dim.n = 2 ; % number of hidden states
dim.n_theta = 0 ; % number of evolution parameters
dim.n_phi = 1 ; % number of observation parameters

% y = finalPosition;
% x = posteriorTarget;
u = data.tableData.advicePosition;
alpha = inf;
sigma = 1e2;

[y, x] = VBA_simulate (n_t, f_fname, g_fname, theta, phi, u, alpha, sigma, [], x0);
