function [LL] = LLexNoDerivative(e,x,y,grid)
% log-likelihood of e and x: log p(y|x,e)
% function [LL] = LLex(e,x,y,P)
% IN:
%   - e: the values of e at which log p(y|x,y) is evaluated
%   - x: the values of x at which log p(y|x,y) is evaluated
%   - y: the values of y at which log p(y|x,y) is evaluated
%   - P: parameter structure
% OUT:
%   - LL: log-likelihood: log p(y|x,e)

% benevolent likelihood function: log p(y|x,Inf)
lp1 = logGauss(y,x,grid.sigma,grid.d.y);
p1 = exp(lp1-max(lp1(:)))*exp(max(lp1(:))); % for numerical stability
% dP1dSigma = ((x-y-sqrt(grid.sigma)).*(x-y+sqrt(grid.sigma))/grid.sigma).*p1;

alpha = grid.alpha;
beta = grid.beta;
% dAlphadSigma = grid.dAlphadSigma;
% dBetadSigma = grid.dBetadSigma;

% p(y|x,e) and log(p(y|x,e))
sigE =  1 / (1 + exp(-e*grid.slope));
py = sigE.*p1 + (1-sigE).*(alpha-beta*p1);
py(py<0)=eps;
LL = log(py);

% dp(y|x,e)/dSigmaAdvice and then d(log(py))dSigma
% dPydSigma = sigE.*dP1dSigma + (1 - sigE).*(dAlphadSigma - dBetadSigma*p1 - dP1dSigma*beta);
% dLLdSigma = dPydSigma./py;

end


