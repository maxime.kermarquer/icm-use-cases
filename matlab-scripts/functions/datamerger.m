%% Data merger

datadir = 'C:\Users\jacob.dhote\Desktop\ketabi\data';
listFile = dir(datadir);

expression1 = '\w*ketabiFaceAdvice\w*';
expression2 = '\w*session_1\w*';
for iFile = 1:length(listFile)
        a(iFile) = isempty(regexp(listName{iFile}, expression1, 'match'))==0;   % only ketabiFaceAdvice 
        b(iFile) = isempty(regexp(listName{iFile}, expression2, 'match'))==0;   % no training sessions
end
listFaceAdvice = listFile(a==1 & b==1); % list of files for ketabiFaceAdvice experiment, without training

%% big table with everything
tableResults = table;
for iFile = 1:length(listFaceAdvice)
    load (listFaceAdvice(iFile).name);
    tableResults.(strcat('subject_', num2str(iFile))) = data.tableData;
    tableResults.
%     b(iFile) = data.trial;
end