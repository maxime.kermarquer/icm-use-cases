function [ V ] = linearize( M )
%% V = M(:);
% Useful to limite creation of temporary variable

V = M(:);

end

