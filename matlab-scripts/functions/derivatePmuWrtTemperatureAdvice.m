function [dMuFacedFace,dSigmaFacedFace] = derivatePmuWrtTemperatureAdvice(JP,dJPdTemperatureAdvice,grid)

    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(dJPdTemperatureAdvice, 1),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPmu = (dDoubleSumJP*tripleSumJP - dTripleSumJP*doubleSumJP) / tripleSumJP^2;

    dMuFacedFace = grid.gmu * dNormalizedPmu;
    dSigmaFacedFace = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
        / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));

end

