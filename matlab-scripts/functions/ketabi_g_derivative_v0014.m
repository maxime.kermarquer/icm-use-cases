function [dgdx,dgdp] = ketabi_g_derivative_v0014(gx,inG,matX,temperatureShoot,isSoftmaxPx)

%% dgdx
sizeDgdx = [length(matX) length(gx)];
dgdx = zeros(sizeDgdx);

% confidence
dgdx(13,1) = 5;

% bins
bin = matX((end-inG.nBin+1):end);
if isSoftmaxPx == 1
    dgdx(end-4:end,end-4:end) = -gx(2:end)*gx(2:end)'/temperatureShoot;
    for i = 1:inG.nBin  
        dgdx(end-5+i,end-5+i) = gx(1+i)*(1-gx(1+i))/temperatureShoot;
    end
elseif isSoftmaxPx == 0
    dgdx(end-4:end,end-4:end) = eye(5);
end

%% dgdp
sizeDgdp = [1 length(gx)];
dgdp = zeros(sizeDgdp);
for iBin = 1:inG.nBin
    u = exp(bin(iBin)/temperatureShoot);
    v = sum(exp(bin/temperatureShoot));
    dUdTemperature = -bin(iBin)*exp(bin(iBin)/temperatureShoot)/temperatureShoot^2;
    dVdTemperature = -sum(bin.*exp(bin/temperatureShoot))/temperatureShoot^2;
    dgdp(iBin+1) = (dUdTemperature * v - u * dVdTemperature) / v^2 * temperatureShoot;   % multiply by temperatureShoot as it is elevated to the exponential in @g
end

end

