    %% Runs model-based analyses for ketabi face advice experiment for one participant (i)
% v0001 : Evolution through the computation of the joint probaility p(x, e, mu | yx, ye)
% fit of position and confidence, bins of equal number of y hits, confidence as sigmoid(posterior at shoot position)
% with intertrial noise
% with update of disposition based on outcome (not facial expression only)

%% Get version
ketabiAdviceVersion = 'v0001';

homedir   = [filesep 'network' filesep 'lustre' filesep 'iss01' filesep 'mbb' filesep 'raw' filesep 'jacob.dhote' filesep 'cluster' filesep];
datadir   = [homedir, 'simulationConfidenceData'];
resultdir = [homedir, 'resultdir'];
scriptdir = [homedir, 'scriptdir'];
% datadir = '\\lexport\iss01.mbb\raw\jacob.dhote\cluster\simulationConfidenceData'; % if working from outside of cluster
% resutdir = '\\lexport\iss01.mbb\raw\jacob.dhote\cluster\resultdir';
addpath(datadir);
addpath(genpath(scriptdir));

%% Get the indexation of subjects
listFile = dir([datadir filesep 'simulatedDataConfidence*']);
nFile = length(listFile);

parfor iFile = 1:nFile
    fileName = listFile(iFile).name;
    data = getfield(load([datadir filesep fileName]),'data');
    data.simulatedData.confidence = cellfun(@mean, {data.trial.confidence}'); 
    nT = height(data.simulatedData); % change to reduce the number of trials
    data.simulatedData = data.simulatedData(1:nT,:); % Reduced number of T (demo version)

    subjectMatlabNumber = str2double(regexp(fileName, '\d{3}', 'match', 'once'));

    groupName = 'simulatedDataConfidence';
    tableResultFull = table;
    tableResultFull.subjectMatlabNumber = repmat(subjectMatlabNumber, 1);
    tableResultFull.iFile = iFile;
    if sum(ismember(data.simulatedData.faceIdentity{1}, 'L')) == 0
        stimulus = 'face';
    elseif sum(ismember(data.simulatedData.faceIdentity{1}, 'L')) == 1
        stimulus = 'lightbulb';
    end

    %% Define function
    f_fname = @f_ketabiFaceAdvice_0001;   % Evolution function   (that actually does all the job)
    g_fname = @g_ketabiFaceAdvice_0001;   % Observarion function (it just reads the correct input)

    %% Run
    options=struct;
    dim = struct('n',0, 'n_theta', 0, 'n_phi', 0);

    %% Define y, isYOut (remove outlier values)

    nBin = 10;
    y = zeros(nBin + 1, length(data.simulatedData.confidence));
    temp = NaN(length(data.simulatedData.confidence) / nBin , nBin);
    
    % 1st and only source : confidence
    y = data.simulatedData.confidence';
       
    sortedData = sort(data.simulatedData.finalPosition);
    temp = reshape(sortedData, [length(data.simulatedData.confidence) / nBin , nBin]);
    minBin = temp(1,:);
    
    options.isYout = isnan(y);
    y(options.isYout)=0;
    
    nTrial = size(y, 2);
    dim.nT = nTrial;
    options.skipf = zeros(1, nTrial);     % Skip evolution fonction for these trial
    dim.p = size(y, 1);
    
    options.extended = 0;
    options.sources.out  = 1;
    options.sources.type = 0;

    %% Define prior for hidden states
    % Mu & sigma for each face
    temp = varfun(@mean, data.simulatedData, 'GroupingVariables', {'faceNumber', 'faceIdentity', 'faceContingency'}, 'InputVariable', {'finalPosition'});
    listBin = {'bin1', 'bin2', 'bin3', 'bin4', 'bin5', 'bin6', 'bin7', 'bin8', 'bin9', 'bin10'};
    listX = [strcat('muFace_', temp.faceIdentity) ; strcat('sigmaFace_', temp.faceIdentity) ; 'confidence' ; listBin']';
    muX0 =   [zeros(1, 5), zeros(1, 5), 0, zeros(1,nBin)]'  ;
    sigmaX0 = [zeros(1, 5), zeros(1, 5), 0, zeros(1,nBin)];
    
    [dim, options] = setListPriors(options, dim, 'X0', listX, muX0, sigmaX0);

    %% Define priors for evolution parameters
    [dim, options] = setPriors(options, dim, 'theta',...
        'muFace_0',                     -0.0672, 0,...           % mean of prior on the intention of people in general
        'sigmaFace_0',                  -1.6969, 0,...           % std of prior on the intention of people in general
        'muTarget',                     26, 0,...                % mean of prior of target position (fixed in this version of the model)
        'sigmaTarget',                  2.6882, 0,...      % std of prior of target position
        'sigmaAdvice',                  1.0736, 0,...     % precision of the advice(range where the advice gives information)
        'sigmaFace_percept',            0.7158, 0,...       % precision of the emotion depending of the facial expression
        'temperatureAdvice',            2.3060, 0,...      % inverse temperature of the sigmoid used to transform e into b
        'gamma',                        0.3472, 0,...       % precision of the prior of the emotion displayed by an individual
        'interTrialNoise',              -7.8242, 0,...     % noise to allow sigmaFace to change from 1 trial to another
        'temperatureConfidence',        log(.05), 1,...     % temperature of the dependance between confidence and probability of success
        'shiftConfidence',              2.5, 1);              % shift of the dependance between confidence and probability of success

    %% Define priors for observation parameters
    [dim, options] = setPriors(options, dim, 'phi');%,...        
%         'temperatureShoot',             log(.1), 1);        % temperature of the dependance between choice of shoot position and probability of success

    %% Define options (in)
    options.inF.listFaceIdentity     = temp.faceIdentity;
    options.inF.nBin                 = nBin;
    options.inF.minBin               = minBin;    
    options.inG.nBin                 = nBin;
    
    % State noise precision (only for dynamical systems)
    options.priors.a_alpha = Inf ;
    options.priors.b_alpha = 0 ;

    % Define hyperpriors
    [options.priors.a_sigma(1),options.priors.b_sigma(1)] = VBA_guessHyperpriors(y(1, options.isYout(1,:)==0)) ;

    % Get u
    data.simulatedData.degreeExpression(abs(data.simulatedData.degreeExpression)==1) = 0;
    data.simulatedData.degreeExpressionPercept = str2num(char(changeLabel(data.simulatedData.degreeExpression, [-10 -4 -1 0 1 4 10],  unique(data.simulatedData.degreeExpression))));

    [u, dim, options]=setInput(options,dim,...
        'advicePosition',       data.simulatedData.advicePosition',...            % advice position
        'identityFace',         data.simulatedData.identityFace',...              % face identity
        'degreeExpression',     data.simulatedData.degreeExpressionPercept', ...  % face expression (from -100% to 100% according to perception)
        'iTrial',               1:nTrial, ...                                 % iTrial
        'finalPosition',        data.simulatedData.finalPosition',...
        'targetPosition',       data.simulatedData.targetPosition');

    %% Define options and misc
    options.checkGrads      = 0;
    options.DisplayWin      = 0; % Do you want to display fig during inversion?
    options.GnFigs          = 0;
    options.dim             = dim;
    options.verbose         = 1; % Do you want to display text during inversion ?

    %% Do inversion
    [posterior,out] = VBA_NLStateSpaceModel(y,u,f_fname,g_fname,dim,options);
    if isempty(out)
        error('out is empty : please check');
    end

    %% Get free energy
    tableResultFull.F = out.F;

    %% Store full results
    tableResultFull.posterior = posterior;
    tableResultFull.out = out;

    tableResultSummary = tableResultFull;
    tableResultSummary.posterior = [];
    tableResultSummary.out = [];
    summaryName = [resultdir filesep ketabiAdviceVersion '_' groupName '_' num2str(subjectMatlabNumber) '_' stimulus '_summary_' strrep(strrep(datestr(clock), ' ', '-'), ':', '-') '.mat'];
%     save(summaryName, 'tableResultSummary');
    resultName  = [resultdir filesep ketabiAdviceVersion '_' groupName '_' num2str(subjectMatlabNumber) '_' stimulus '_full_' strrep(strrep(datestr(clock), ' ', '-'), ':', '-') '.mat'];
    saveToFile(resultName, tableResultFull);
end