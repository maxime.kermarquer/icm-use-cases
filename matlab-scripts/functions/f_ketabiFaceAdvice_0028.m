function [updatedX,dfdx,dfdp] = f_ketabiFaceAdvice_0028(x_t,P,u_t,inF)
%% function [updatedX] = f_ketabiFaceAdvice_0028(x_t,P,u_t,inF)
%
% INPUT
% - x_t : 
%           muFace_(faceIdentity)     = mean of the prior distribution for faceIdentity
%           sigmaFace_(faceIdentity)  = variance of the prior distribution for faceIdentity
%           confidence                = confidence derived from the shooting position, to be carried out to g_
%           bin(i)                    = posterior distribution of target position divided in nBin bins
% - P : 
%             muFace_0              = mean of prior on the intention of people in general
%             sigmaFace_0           = std of prior on the intention of people in general
%             muTarget              = mean of the prior distribution for target position
%             sigmaTarget           = std of the prior distribution for target position
%             sigmaAdvice           = std of the advice(range where the advice gives information)
%             sigmaFace_percept     = std of the emotion
%             temperatureAdvice     = temperature of the sigmoid used to transform e into b
%             gamma                 = std of the prior of the intention displayed by an individual
%             temperatureConfidence = temperature of the dependance between confidence and probability of success
%             shiftConfidence       = shift of the dependance between confidence and probability of success
% - u_t :
%           advicePosition       
%           identityFace     
%           degreeExpression       
%           iTrial                      
% - inF : 
%           listFaceIdentity : a cell with all face identitity in the correct order (faceNumber)
%           nBin : number of bins to divide the posterior distribution of the target position
%           isFinalPosition = use real shooting position to derive confidence if (1), use best simulated position if (0)
% OUTPUT
% - fx :   Updated X



%% Get parameters and u (experimental conditions)
%--------------------------------------------------------------------------
[X,theta,u] = getStateParamInput(x_t,P, u_t,inF);
currentFace = inF.listFaceIdentity{u.identityFace};

%% Set parameters
% initialize grids 
grid.d.x    = 1; % x: grid step size
grid.d.e    = 1e-1; % e: grid step size
grid.d.mu   = 1e-1; % mu: grid step size
grid.d.y    = 1e0; % y: grid step size
grid.d.f    = 1e-1; % f: grid step size
grid.gx     = -11:grid.d.x:63; % x: grid
grid.ge     = -5:grid.d.e:5; % e: grid
grid.gmu    = -5:grid.d.mu:5; % mu: grid
grid.gy     = -11:grid.d.y:63;  % used to determine alpha and beta

% model parameters
grid.muTarget       = theta.muTarget;
grid.sigmaTarget    = exp(theta.sigmaTarget)^2; % std -> variance
grid.gamma          = exp(theta.gamma)^2;  % std -> variance
grid.ohmega         = exp(theta.sigmaFace_percept)^2; % std -> variance
grid.sigma          = exp(theta.sigmaAdvice)^2; % std -> variance
grid.slope          = exp(theta.temperatureAdvice);      
interTrialNoise = exp(theta.interTrialNoise);
temperatureConfidence = exp(theta.temperatureConfidence);
shiftConfidence = theta.shiftConfidence;

% parameters alpha and beta are determined to build the right inverse gaussian
py0 = exp(logGauss(grid.gy,27,grid.sigma,grid.d.y));
coeffSoftmax = 1e3;
softmaxPy0 = log(sum(exp(coeffSoftmax*py0)))/coeffSoftmax;
grid.alpha = 1/sum(1-py0/softmaxPy0);
grid.beta = 1/(sum(1-py0/softmaxPy0)*softmaxPy0);

dPy0dSigma = ((grid.gy-27-sqrt(grid.sigma)).*(grid.gy-27+sqrt(grid.sigma))/grid.sigma).*py0;
dSoftmaxdSigma = 1/coeffSoftmax * 1/sum(exp(coeffSoftmax*py0)) * (sum(coeffSoftmax*dPy0dSigma.*exp(coeffSoftmax*py0)));
grid.dAlphadSigma = sum((dPy0dSigma.*softmaxPy0 - py0.*dSoftmaxdSigma)/softmaxPy0^2) * 1 / (sum(1-py0/softmaxPy0)^2);

w = sum(1-py0/softmaxPy0);
v = softmaxPy0;
dwdSigma = -sum((dPy0dSigma.*softmaxPy0 - dSoftmaxdSigma.*py0)/dSoftmaxdSigma^2);
dvdSigma = dSoftmaxdSigma;
grid.dBetadSigma = -(dwdSigma.*v + dvdSigma.*w)/(w.*v)^2;

% set prior density on mu
if u.iTrial == 1
    for iFace = 1:length(inF.listFaceIdentity)
        X.(['muFace_' inF.listFaceIdentity{iFace}]) = theta.muFace_0;
        X.(['sigmaFace_' inF.listFaceIdentity{iFace}]) = exp(theta.sigmaFace_0);
    end
end
muFace = X.(['muFace_' currentFace]);
sigmaFace = X.(['sigmaFace_' currentFace]);
P0mu = normpdf(grid.gmu, X.(['muFace_' currentFace]), X.(['sigmaFace_' currentFace]));
P0mu = P0mu./sum(P0mu);
P0mu(P0mu==0)=eps;

%% Compute p(x,e,mu|y,f)
% form 3D-grids...
GX = repmat(grid.gx(:),[1,length(grid.ge),length(grid.gmu)]);
GE = repmat(grid.ge(:)',[length(grid.gx),1,length(grid.gmu)]);
GMU = repmat(permute(grid.gmu(:),[2,3,1]),[length(grid.gx),length(grid.ge),1]);
% ... and 3D-prior
P0MU = repmat(permute(P0mu(:),[2,3,1]),[length(grid.gx),length(grid.ge),1]);
% evaluate p(x,e,mu,y,f)
[LJ,dLLexydSigma,dLLdTemperatureAdvice] = logJoint(GX,GE,GMU,log(P0MU),u.advicePosition,u.degreeExpression,grid);
JP = exp(LJ-max(LJ(:))).*exp(max(LJ(:)));
dJPdSigma = JP.*dLLexydSigma; % derivative w.r.t. precision of advice
dJPdTemperatureAdvice = JP.*dLLdTemperatureAdvice; % derivative w.r.t. temperature of advice (how the positive and the negative gaussians are combined in LLex.m)
% marginalize p(x,e,mu|y,f) to get posterior belief on x
px = sum(sum(JP,3),2);
px = px./sum(px);

%% Bin px and save in X
nBin = inF.nBin; % number of bins
minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
rangePosition = -11:63; % possible positions during experiment
binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
for iBin = 1:nBin
    X.(['bin',num2str(iBin)]) = sum(px(binPosition(iBin) : binPosition(iBin + 1)));
end

%% Derive confidence and save in X
rangePosition = -11:63; % possible positions on screen during experiment
shootPosition = u.finalPosition + abs(rangePosition(1)) + 1; % we use real shooting position
shootConfidence = px(round(shootPosition));
X.confidence = 1 / (1 + exp(-shootConfidence / temperatureConfidence + shiftConfidence));

%% Get pmu and save moments in X +/- add noise to precision
% form 2D grids and 2D prior (dimension of position of target is now known, so not a hidden state)
GE2 = repmat(grid.ge(:),[1,length(grid.gmu)]);
GMU2 = repmat(permute(grid.gmu(:),[2,1]),[length(grid.ge),1]);
P0MU2 = repmat(permute(P0mu(:),[2,1]),[length(grid.ge),1]);
% evaluate p(e,mu|y,f,x)
[LJ2,dLLexy2dSigma,dLL2dTemperatureAdvice] = logJoint2(GE2,GMU2,log(P0MU2),u.targetPosition,u.advicePosition,u.degreeExpression,grid);
JP2 = exp(LJ2-max(LJ2(:))).*exp(max(LJ2(:)));
dJP2dSigma = JP2.*dLLexy2dSigma; % derivative w.r.t. precision of advice
dJP2dTemperatureAdvice = JP2.*dLL2dTemperatureAdvice; % derivative w.r.t. temperature of advice (how the positive and the negative gaussians are combined in LLex.m)
% marginalize p(e,mu|y,f,x) to get posterior belief on mu
pmu = sum(JP2,1)';
pmu = pmu./sum(pmu);
X.(['muFace_' currentFace]) = grid.gmu * pmu; % save mean of disposition
X.(['sigmaFace_' currentFace]) = sqrt((grid.gmu).^2 * pmu - (grid.gmu * pmu) ^2); % save std of disposition
% add noise to sigmaFace of all advisors
temp = fieldnames(X);
for i=1:5
    X.(temp{i+5}) = X.(temp{i+5}) + interTrialNoise;
end

%% Derivatives
derivativeArgs = {...
    muFace,sigmaFace,...                                    % moments of disposition before update                
    X,theta,u,grid,inF,...                                  % hidden states, parameters, inputs and options
    GX,GE,GMU,JP,dJPdSigma,dJPdTemperatureAdvice,...        % grids and joint probability used for px
    GE2,GMU2,JP2, dJP2dSigma,dJP2dTemperatureAdvice,...     % grids and joint probability used for pmu
    shootPosition,shootConfidence};                         % for derivatives of confidence
[dfdx,dfdp] = ketabi_f_derivative_v0028(derivativeArgs);

%% Update
updatedX = table2array(struct2table(X))';

