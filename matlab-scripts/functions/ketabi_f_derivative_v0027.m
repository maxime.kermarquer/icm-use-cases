function [dfdx,dfdp] = ketabi_f_derivative_v0027(muFace,sigmaFace,X,theta,u,grid,inF,GX,GE,GMU,JP,dJPdSigma,dJPdTemperatureAdvice,shootPosition,shootConfidence)


currentFace = inF.listFaceIdentity{u.identityFace};
muFace_0 = theta.muFace_0;
sigmaFace_0 = exp(theta.sigmaFace_0);
muTarget = theta.muTarget;
sigmaTarget = exp(theta.sigmaTarget);
sigmaAdvice = exp(theta.sigmaAdvice);
sigmaFace_percept = exp(theta.sigmaFace_percept);
temperatureAdvice = exp(theta.temperatureAdvice);
gamma = sqrt(grid.gamma);
interTrialNoise = exp(theta.interTrialNoise);
temperatureConfidence = exp(theta.temperatureConfidence);
shiftConfidence = theta.shiftConfidence;
nBin = inF.nBin;

%% dfdx
sizeDfdx = length(fieldnames(X));
dfdx = zeros(sizeDfdx);

% derivative w.r.t. muFace
if u.iTrial~=1
    for iFace=1:10
        dfdx(iFace,iFace)=1;
    end
    [dfdx(u.identityFace,u.identityFace),dfdx(u.identityFace,u.identityFace+5)] = derivatePmuWrtNonExpParameter(muFace,sigmaFace,GMU,JP,grid,0);
    dfdx(u.identityFace,end-nBin:end) = derivatePxWrtNonExpParameter(muFace,sigmaFace,GMU,JP,inF,0,shootPosition,X,temperatureConfidence);
end

% w.r.t sigmaFace
if u.iTrial~=1
    % derivate pmu      
    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(JP.*...
        (((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace))/sigmaFace^3 +...
        ((GE-GMU-gamma).*(GE-GMU+gamma))/gamma^3),1),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPmu = (dDoubleSumJP*tripleSumJP - dTripleSumJP*doubleSumJP) / tripleSumJP^2;
    dfdx(u.identityFace+5,u.identityFace) = grid.gmu * dNormalizedPmu;  
    dfdx(u.identityFace+5,u.identityFace+5) = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
            / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));
        
    % derivate px
    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    dDoubleSumJP = VBA_vec(sum(sum(JP.*...
        (((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace))/sigmaFace^3 +...
        ((GE-GMU-gamma).*(GE-GMU+gamma))/gamma^3),3),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;
    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end
    % put together
    dfdx(u.identityFace+5,end-nBin:end)=[dSigmoidConfidence dBin];

    
end

%% dfdp
sizeDfdp = length(fieldnames(theta));
dfdp = zeros(sizeDfdp,sizeDfdx);

% w.r.t. muFace_0
if u.iTrial == 1
    dfdp(1,1:5) = 1;
    [dfdp(1,u.identityFace), dfdp(1,u.identityFace+5)] = derivatePmuWrtExpParameter(muFace,sigmaFace,GMU,JP,grid,0);
    dfdp(1,end-nBin:end) = derivatePxWrtExpParameter(muFace,sigmaFace,GMU,JP,inF,0,shootPosition,X,temperatureConfidence);
end
    
% w.r.t. sigmaFace_0
if u.iTrial == 1
    dfdp(2,6:10) = sigmaFace_0;
    
    % derivate pmu      
    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(JP/sigmaFace^2.*...
        (((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace)) +...
        ((GE-GMU-gamma).*(GE-GMU+gamma))),1),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPmu = (dDoubleSumJP*tripleSumJP - dTripleSumJP*doubleSumJP) / tripleSumJP^2;
    dfdp(2,u.identityFace) = grid.gmu * dNormalizedPmu;  
    dfdp(2,u.identityFace+5) = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
            / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));
        
    % derivate px
    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    dDoubleSumJP = VBA_vec(sum(sum(JP/sigmaFace^2.*...
        (((GMU-muFace-sigmaFace).*(GMU-muFace+sigmaFace)) +...
        ((GE-GMU-gamma).*(GE-GMU+gamma))),3),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;
    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end
    % put together
    dfdp(2,end-nBin:end) = [dSigmoidConfidence dBin];

end

% w.r.t. muTarget
[dfdp(3,u.identityFace), dfdp(3,u.identityFace+5)] = derivatePmuWrtExpParameter(muTarget,sigmaTarget,GX,JP,grid,0);
dfdp(3,end-nBin:end) = derivatePxWrtExpParameter(muTarget,sigmaTarget,GX,JP,inF,0,shootPosition,X,temperatureConfidence);

% w.r.t. sigmaTarget
[dfdp(4,u.identityFace), dfdp(4,u.identityFace+5)] = derivatePmuWrtExpParameter(muTarget,sigmaTarget,GX,JP,grid,1);
dfdp(4,end-nBin:end) = derivatePxWrtExpParameter(muTarget,sigmaTarget,GX,JP,inF,1,shootPosition,X,temperatureConfidence);

% w.r.t. sigmaAdvice 
[dfdp(5,u.identityFace), dfdp(5,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdSigma,grid);
dfdp(5,end-nBin:end) = dShootPxWrtAdvice(JP,dJPdSigma,inF,shootPosition,X,temperatureConfidence);

% w.r.t. sigmaFacePercept
[dfdp(6,u.identityFace), dfdp(6,u.identityFace+5)] = derivatePmuWrtExpParameter(u.degreeExpression,sigmaFace_percept,GE,JP,grid,1);
dfdp(6,end-nBin:end) = derivatePxWrtExpParameter(u.degreeExpression,sigmaFace_percept,GE,JP,inF,1,shootPosition,X,temperatureConfidence);

% % w.r.t. slope (inverse temperature) of advice
[dfdp(7,u.identityFace), dfdp(7,u.identityFace+5)] = derivatePmuWrtAdvice(JP,dJPdTemperatureAdvice,grid);
dfdp(7,end-nBin:end) =  dShootPxWrtAdvice(JP,dJPdTemperatureAdvice,inF,shootPosition,X,temperatureConfidence);

% % w.r.t. gamma (intentional variability)
% [dfdp(8,u.identityFace), dfdp(8,u.identityFace+5)] = derivatePmuWrtExpParameter(GMU,gamma,GE,JP,grid,1);
% dfdp(8,end-nBin:end) = derivatePxWrtExpParameter(GMU,gamma,GE,JP,inF,1,shootPosition,X,temperatureConfidence);

% w.r.t. intertrial noise
dfdp(8,6:10) = interTrialNoise;

% wrt confidence temperature
dfdp(9,end-nBin) = -X.confidence*(1-X.confidence)*(shootConfidence)/temperatureConfidence; % wrt temperature
dfdp(10,end-nBin) = -X.confidence*(1-X.confidence);   % wrt shift

end

function [derivatives] = derivatePxWrtExpParameter(mu,sigma,x,JP,inF,isSigma,shootPosition,X,temperatureConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu)./sigma^2),3),2));
    elseif isSigma==1
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu-sigma).*(x-mu+sigma))/sigma^2,3),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end

function [derivatives] = derivatePxWrtNonExpParameter(mu,sigma,x,JP,inF,isSigma,shootPosition,X,temperatureConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP,3),2)); % this is px
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for px
    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu)./sigma^2),3),2));
    elseif isSigma==1
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu-sigma).*(x-mu+sigma))/sigma^3,3),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;

    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end

function [dMuFacedFace,dSigmaFacedFace] = derivatePmuWrtExpParameter(mu,sigma,x,JP,grid,isSigma)
    
    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu

    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu)./sigma^2),1),2));
    elseif isSigma==1
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu-sigma).*(x-mu+sigma))/sigma^2,1),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPmu = (dDoubleSumJP*tripleSumJP - dTripleSumJP*doubleSumJP) / tripleSumJP^2;

    dMuFacedFace = grid.gmu * dNormalizedPmu;  
    dSigmaFacedFace = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
            / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));
        
end

function [dMuFacedFace,dSigmaFacedFace] = derivatePmuWrtNonExpParameter(mu,sigma,x,JP,grid,isSigma)
    
    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu

    if isSigma==0
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu)./sigma^2),1),2));
    elseif isSigma==1
        dDoubleSumJP = VBA_vec(sum(sum(JP.*((x-mu-sigma).*(x-mu+sigma))/sigma^3,1),2));
    end
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPmu = (dDoubleSumJP*tripleSumJP - dTripleSumJP*doubleSumJP) / tripleSumJP^2;

    dMuFacedFace = grid.gmu * dNormalizedPmu;  
    dSigmaFacedFace = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
            / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));
        
end

function [derivatives] = dShootPxWrtAdvice(JP,dJPdTheta,inF,shootPosition,X,temperatureConfidence)

    doubleSumJP = VBA_vec(sum(sum(JP, 3),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(dJPdTheta, 3),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPx = (dDoubleSumJP .* tripleSumJP - dTripleSumJP.*doubleSumJP) / tripleSumJP^2;

    % confidence
    dShootConfidence = dNormalizedPx(round(shootPosition));
    dSigmoidConfidence = dShootConfidence*X.confidence*(1-X.confidence)/temperatureConfidence;
    
    % bins
    nBin = inF.nBin; % number of bins
    minBin = inF.minBin; % bottom bound for each bin in -11:63 (experiment) referential
    rangePosition = -11:63; % possible positions on screen during experiment
    binPosition = floor([rangePosition(1) minBin(2:end) rangePosition(end)] + abs(rangePosition(1)) + 1); % bounds for bins in 1:75 referential
    for iBin = 1:nBin
        dBin(iBin) = sum(dNormalizedPx(binPosition(iBin) : binPosition(iBin + 1)));
    end

    % put together
    derivatives=[dSigmoidConfidence dBin];

end

function [dMuFacedFace,dSigmaFacedFace] = derivatePmuWrtAdvice(JP,dJPdTheta,grid)

    doubleSumJP = VBA_vec(sum(sum(JP, 1),2)); % this is pmu
    tripleSumJP = sum(doubleSumJP); % this is normalization constant for pmu
    dDoubleSumJP = VBA_vec(sum(sum(dJPdTheta, 1),2));
    dTripleSumJP = sum(dDoubleSumJP);
    dNormalizedPmu = (dDoubleSumJP*tripleSumJP - dTripleSumJP*doubleSumJP) / tripleSumJP^2;

    dMuFacedFace = grid.gmu * dNormalizedPmu;
    dSigmaFacedFace = (grid.gmu.^2 * dNormalizedPmu - 2 * (grid.gmu*doubleSumJP / tripleSumJP)*(grid.gmu*dNormalizedPmu))...
        / (2 * sqrt((grid.gmu).^2 * (doubleSumJP / tripleSumJP) - (grid.gmu * (doubleSumJP / tripleSumJP)).^2));

end


