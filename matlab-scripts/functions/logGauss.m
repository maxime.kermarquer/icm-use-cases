function lp = logGauss(x,m,v,dx)
% evaluates the log of a Gaussian densitywith mean m and variance v (at x)
lp = -0.5*(((x-m).^2)./v) - 0.5*log(2*pi*v) + log(dx);